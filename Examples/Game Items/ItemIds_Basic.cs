﻿namespace Meep.Tech.ModularData.Examples.Game_Items {

  /// <summary>
  /// This is a collection example Item.Ids for the Base of a game
  /// </summary>
  public class ItemIds_Basic : Item.Id {

    /// <summary>
    /// This will be a basic item Id for an item of type "Items.Basic.Apple"
    /// </summary>
    public static ItemIds_Basic Apple {
      get;
      // Make sure to set as default, not => get as that will make a new object each time.
    } = new ItemIds_Basic("Apple");

    /// <summary>
    /// This will be a basic item Id for an item of type "Items.Basic.Rock"
    /// </summary>
    public static ItemIds_Basic Rock {
      get;
      // also make sure to use the current type as the CTOR for naming conventions, and for sorting with pattern matching.
    } = new ItemIds_Basic("Rock");

    /// <summary>
    /// Make this constructor extensible just because:
    /// </summary>
    protected ItemIds_Basic(string name, string externalIdEnding = null) 
      : base($"Basic.{externalIdEnding ?? name}", name) {}
  }
}
