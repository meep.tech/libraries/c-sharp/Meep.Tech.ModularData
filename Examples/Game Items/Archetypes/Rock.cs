﻿namespace Meep.Tech.ModularData.Examples.Game_Items {

  // Some Archetypes can be very simple
  public class Rock : Item.Type {
    protected Rock() 
      : base(false, ItemIds_Basic.Rock) {}
  }
}
