﻿namespace Meep.Tech.ModularData.Examples.Game_Items {

  /// <summary>
  /// An Fruit Type of an Apple.
  /// </summary>
  public class Apple : Fruit.Type {

    /// <summary>
    /// Can access this via:
    /// (Item.Types.Get(ItemIds_Basic.Apple) as Fruit).Color
    /// </summary>
    public override Item.Color Color
      => Item.Color.Red;

    /// <summary>
    /// Protected so it can be extended but only made internally
    /// </summary>
    protected Apple() 
      : base(ItemIds_Basic.Apple) {}
  }
}
