﻿using System;
using System.Collections.Generic;

namespace Meep.Tech.ModularData.Examples.Game_Items {

  /// <summary>
  /// An Item Model representing a Fruit
  /// </summary>
  public class Fruit : Item {

    /// <summary>
    /// The ripeness of this fruit
    /// </summary>
    [ModelDataField]
    public int ripeness {
      get;
      internal protected set;
    }

    /// <summary>
    /// An protected constructor
    /// </summary>
    protected internal Fruit(Fruit.Type type) 
      : base(type) {}

    /// <summary>
    /// We can make a new base item archtype for just Fruits!
    /// </summary>
    public abstract new class Type : Item.Type {

      /// <summary>
      /// Default color of this item used for consturction, and access via:
      /// (Item.Types.Get(ItemIds_Basic.Apple) as Apple).Color
      /// </summary>
      public abstract Item.Color Color {
        get;
      }

      /// <summary>
      /// You can override the base cost like this, even using it and adding to it:
      /// </summary>
      public override int CostInGoldPoints
        => base.CostInGoldPoints + 5;

      /// <summary>
      /// How to make a fruit of any kind:
      /// </summary>
      public Type(Item.Id typeId) : base(
        // we can override this to true here because all fruit are edible
        true, 
        typeId
      ) { }

      /// <summary>
      /// This makes a Fruit as a model, not a generic Item
      /// </summary>
      protected internal override Item ModelConstructor(Item.Type type)
        => new Fruit(type as Fruit.Type);

      /// <summary>
      /// This updates how the model is configured by default:
      /// </summary>
      protected override Item ConfigureModel(Item newModel, Dictionary<Param, object> @params) {
        // you can use Merge and a new dictionary of params to pass defaults to the base model config:
        Fruit fruit = base.ConfigureModel(newModel, @params.Merge(new Dictionary<Param, object> {
          { Item.Type.Params.Color, Color }
        })) as Fruit;

        // adding new params to the model is pretty straitforward:
        fruit.ripeness = @params.GetAs(ItemTypeParams_Apple.Ripeness, 10);

        return fruit;
      }
    }
  }

  /// <summary>
  ///  you don't need to extend params nested inside of any spefici classes, but you can if you want.
  ///  This one is outside the classes this time, but could also go in Fruit.Type.Params.
  /// </summary>
  public class ItemTypeParams_Apple : Item.Type.Params {

    /// <summary>
    /// This is so we can pass in ripeness when making a new item.
    /// Note that params can be nullable:
    /// </summary>
    public static ItemTypeParams_Apple Ripeness = new ItemTypeParams_Apple("Ripeness", typeof(int?));

    /// <summary>
    /// Extend the ctor so these params will be "Params.Items.Apple.Paramname"
    /// </summary>
    protected ItemTypeParams_Apple(string name, Type type, string externalIdEnding = null)
       : base(name, type, $"Fruit.{externalIdEnding ?? name}") { }
  }
}
