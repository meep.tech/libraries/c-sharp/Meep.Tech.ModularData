﻿using System;
using System.Collections.Generic;

namespace Meep.Tech.ModularData.Examples.Game_Items {

  /// <summary>
  /// A flyweight model representing the base types for a video game item.
  /// </summary>
  // Dictates that IModel.Save() will save models of this type to the Items table in sqlite.
  [DataModel("Items")]
  public class Item : IUniqueModel<Item.Type> {

    /// <summary>
    /// Item color enum used for examples
    /// </summary>
    public enum Color {
      None,
      Red,
      Yellow,
      Blue
    }

    /// <summary>
    /// This is the ArchetypeCollection, An enumerable collection of all Archetype Singletons of Type Item.Type
    /// </summary>
    public static ArchetypeCollection<Item.Type, Item.Id, Item> Types {
      get;
      // if you don't want to provide a "None" type you can just use null as the none type and pass null here instead:
    } = new ArchetypeCollection<Type, Id, Item>(typeof(Item.Type.None));

    /// <summary>
    /// Since this is a Unique Model it needs a Unique identifier
    /// </summary>
    // Dictates this is the Unique id field for sql column serialization
    [ModelIdField]
    public string id {
      get;
      // internal set is suggested so only the Archetype<,,>.ConfigureModel function has access
      internal set;
    }

    /// <summary>
    /// The Archetype this Flyweight model represents.
    /// </summary>
    // Dictates this is the Archetype field for sql column serialization
    [ModelTypeField(typeof(Item.Type))]
    public Type type {
      get;
    }

    /// <summary>
    /// The only really unique property of this model,
    /// This would be a base property every archetype of this type would be able to modify or set on a model.
    /// This property would be specific to each model, not to it's archetype (unless the arhcetype has special logic)
    /// Used just for examples.
    /// </summary>
    // Dictates this is a generic supported data type field for serialization to the JSON 'data' column in sql
    [ModelDataField]
    public Color color {
      get;
      // Just like the ID, any property you're going to change only in Archetype<,,>.ConfigureModel should be internal or protected
      internal set;
    }

    /// <summary>
    /// A protected constructor is suggested.
    /// It should only be accesed by the Archetype<,,>.ModelConstructor override below, or by extension model classes.
    /// </summary>
    /// <param name="type">The archetype Unique ID to use to build this model. This should always be set in the ctor</param>
    protected Item(Type type) {
      this.type = type;
    }

    /// <summary>
    /// The Enumeration type to be used as the ID for this Archetype.
    /// The ID type contains just simple name and uuid values for indexing etc.
    /// This doesn't need to go here (inside the model class), and can be in a whole library of it's own in order to support modular access to types by id before implimenting them.
    /// </summary>
    public class Id : Enumeration {
      // Constructor should be protected so only extending classes of Id can use it as seen in ItemIds_Basic.cs
      protected Id(string name, string externalIdEnding = null) 
        // Make sure to include this class as the base type of this new Enumeration:
        : base($"Items.{externalIdEnding ?? name}", name, typeof(Id)) {}

      // It's usually best to provide the empty type Id.
      // This will have the external Id "Items.None"
      public static Id None = new Id("None");
    }

    /// <summary>
    /// The "Archetype" to be used in these flyweight models.
    /// The base Archetype is usually abstract, abstract achetypes are not made into singletons and are not included in the archetype collection.
    /// The Type base class usually goes inside the model to make it easier to find. EX: Model.Type.
    /// </summary>
    public abstract class Type : Archetype<Item.Type, Item.Id, Item> {

      /// <summary>
      /// The value of this Item Type.
      /// This is a property of the Archetype itself, and is not unique to each model, but specific to the type of model it is.
      /// Making this virtual allows sub types/extension classes to impliment their own item value in gold points.
      /// </summary>
      public virtual int CostInGoldPoints
        => 5;

      /// <summary>
      /// Make a "None" Type in the base class for consistency.
      /// Providng a None type to the ArchetypeCollecton will guarentee the local ID of 0 is set to the provided class.
      /// </summary>
      public class None : Item.Type {
        // simple and empty
        protected None() : base(false, Item.Id.None) {}
      }

      /// <summary>
      /// This is a value that all archetypes have, but is set in the CTor data.
      /// This is just an optional way to set archetype data when inheriting as opposed to using virtual properties.
      /// You can make them readonly this way
      /// </summary>
      public bool IsEdible {
        get;
      }

      /// <summary>
      /// Parameters that can be passed to Make to construct a model for this Archetype type.
      /// This would be any base parameters all Models of all Archetypes of this type would be able to use.
      /// </summary>
      public class Params : Param {

        /// <summary>
        /// A singleton parameter so a user can set "Color" via the Archetype<,,>.Make() functionality
        /// </summary>
        public static Params Color = new Params("Color", typeof(Color));

        /// <summary>
        /// This is protected so you can extend the param IDs with each modular extension of the archetype, and to keep them as singletons.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="externalIdEnding"></param>
        protected Params(string name, System.Type type, string externalIdEnding = null)
          // It's custom to just use the name as the ending of the external id for an enum or archetype:
          // since Params base ctor has Params. as a prefix, these will come out to "Params.Items.ItemName" for example
          : base(name, type, $"Items.{externalIdEnding ?? name}") {}
      }

      /// <summary>
      /// This should be protected. Archetypes are treated as singletons so this should only be used for extention classes.
      /// </summary>
      /// <param name="typeId"></param>
      /// <param name="containingCollection"></param>
      protected Type(bool isEdible, Id typeId)
        : base(        
            // typeId Enum is always needed
            typeId,
            // Provide the Archetype Collection for this Archetype here, so all children will be added to it automatically:
            Item.Types
      ) {
        // Put any setup logic for all Archetype Singletons of this type here.
        IsEdible = isEdible;
      }

      /// <summary>
      /// This is used to construct a model for this Archetype type.
      /// You can extend it in child classes to add more model features to the Archetype<,,>.Make() function's functionality
      /// </summary>
      protected override Item ConfigureModel(Item newModel, Dictionary<Param, object> @params) {
        // Get the ID. With this function, it's not requreed and you can provide a default if it's null or missing:
        newModel.id = @params.GetAs(Param.UniqueId, Guid.NewGuid().ToString());
        // Get the Custom param of "Color", this is requried and will throw an exception if it's missing:
        newModel.color = @params.GetAndValidateAs<Color>(Item.Type.Params.Color);

        // Return the model when we're done. Models can be structs sometimes
        return newModel;
      }

      /// <summary>
      /// This is just to link the ctor without needing reflection.
      /// You can also use this to change what type of Item Model a specific Archetype creates using the Archetype<,,>.Make() functions.
      /// </summary>
      protected internal override Item ModelConstructor(Type type)
        => new Item(type);
    }
  }
}
