﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Meep.Tech.ModularData.Mapping {

  /// <summary>
  /// Can be used to collect all classes of a given archetype and map their asset data from external sources to them.
  /// This can also be used with Enumerations to make them like "lite" archetypes
  /// </summary>
  /// <typeparam name="MappableType">The archettype or Enum we're mapping</typeparam>
  /// <typeparam name="DataMapType">Thhe data map, used to store external assets</typeparam>
  public abstract class ArchetypeMapper<MappableType, DataMapType> 
    : ArchetypeMapper
      , IArchetypeMapper<MappableType, DataMapType>
    where DataMapType : ArchetypeDataMap<MappableType> 
    where MappableType : IMappable 
  {

    #region Constants

    #region Abstract

    /// <summary>
    /// The first type to load for this MappableType
    /// </summary>
    protected virtual Enumeration NoneIdType
      => null;

    /// <summary>
    /// Class of the data Map type
    /// </summary>
    public override Type DataMapClass
      => typeof(DataMapType);

    /// <summary>
    /// The full save folder root for this data maper's type data
    /// </summary>
    public override string DataFolderForThisMappableType
      => Path.Combine(SaveDataRootDirectory, DataFolder, MapedTypeDataFolderName);

    /// <summary>
    /// The mapable data type base
    /// </summary>
    internal override Type MappableTypeClass {
      get;
      set;
    }

    #endregion

    #endregion

    /// <summary>
    /// The collected data map
    /// </summary>
    internal Dictionary<int, DataMapType> dataMap
      = new Dictionary<int, DataMapType>();

    /// <summary>
    /// Cache for this type when grabbed.
    /// </summary>
    Type genericVariableInterfaceType;

    #region Initialization

    /// <summary>
    /// Load allthe data types for this type of data
    /// </summary>
    internal override void loadAndMapAllData() {
      MappableTypeClass = getMappableBaseType();

      /// Make sure the first type is registered. Usually for the None type
      if (NoneIdType != null) {
        _ = NoneIdType.Id;
      }

      // load all types of this mappable type
      IEnumerable<Type> allChildrenOfThisMappableType = AppDomain.CurrentDomain.GetAssemblies()
        .SelectMany(assembly => !assembly.IsDynamic && assembly.FullName.StartsWith(ProjectAssemblyFilePrefix) ? assembly.GetExportedTypes() : new Type[0])
        .Where(type => !type.IsAbstract && MappableTypeClass.IsAssignableFrom(type));

      // load the id enums
      if (ConstructAssemblyClasses) {
        typesLeftToConstruct = allChildrenOfThisMappableType.ToDictionary(type => type.FullName);
        Type idType = null;
        // find the type of the ID for this EnumerableType<,>
        foreach (Type constructableType in typesLeftToConstruct.Values) {
          if (!constructableType.IsAssignableToGeneric(typeof(Archetype<,,>))) {
            throw new Exception($"Mapper: {GetType()}, can't construct a class unless it inherits from EnumerableType. Classname: {constructableType.FullName}");
          }

          idType = constructableType.GetInheritedGenericTypes(typeof(Archetype<,,>)).ElementAt(1);
          break;
        }

        // Get the ID types and construct them now on the first run-though
        IEnumerable<Type> idTypes = AppDomain.CurrentDomain.GetAssemblies()
          .SelectMany(assembly => !assembly.IsDynamic && assembly.FullName.StartsWith(ProjectAssemblyFilePrefix)
            ? assembly.GetExportedTypes()
            : new Type[0]
          );

        idTypes
          = idTypes.Where(type => type == idType || idType.IsAssignableFrom(type));

        loadAsSimpleEnums(idTypes, idType);
      } else {
        loadAsSimpleEnums(allChildrenOfThisMappableType);
      }

      /// register the fetch method
      registerMappableTypeFetchMethod(MappableTypeClass);
    }

    /// <summary>
    /// Get the mappable base type for this mapper
    /// </summary>
    /// <returns></returns>
    protected virtual Type getMappableBaseType() =>
      typeof(MappableType);

    /// <summary>
    /// Load the given types from the assembly without mapping their data
    /// </summary>
    void loadAsSimpleEnums(IEnumerable<Type> types, Type baseTypeOverride = null) {
      Type baseType = baseTypeOverride ?? MappableTypeClass;
      foreach (Type type in types) {
        if (baseType.IsAssignableFrom(type)) {
          foreach (PropertyInfo property in type.GetProperties()) {
            if (baseType.IsAssignableFrom(property.PropertyType)) {
              if (property.IsAutoProperty()) {
                Debugger.LogError($"Will not build {baseType.FullName} type with name: {property.Name}, from the property on {type.FullName}. It is an auto-property and will get re-built on each call. Change it into a {"{"}get;{"}"} = [default]; property, with the default being the instantiation(construction) of the singletion type.");
                continue;
              }
              try {
                property.GetValue(null);
              } catch (Exception e) {
                Debugger.LogWarning($"Property error for {property.Name} on {type.FullName}. Make sure the field is a property and the field type is just the base Enum type, ex: ItemId.\n{e.ToString()}");
              }
            }
          }
        }
      }
    }

    /// <summary>
    /// Load the given types from the assembly into a data map for the types
    /// </summary>
    /// <returns>true if no types have failed to build that can be tried again</returns>
    bool loadAsComplexEnumTypes(Dictionary<string, Type> types) {
      bool noTypesFailedToBuild = true;
      genericVariableInterfaceType ??= typeof(IEnumVariableArchetype<>);
      Dictionary<string, Type> remainingTypes = new Dictionary<string, Type>();
      foreach (Type type in types.Values) {
        if (MappableTypeClass.IsAssignableFrom(type)) {
          if (type.IsAssignableToGeneric(genericVariableInterfaceType)) {
            Type subType = type.GetInheritedGenericTypes(genericVariableInterfaceType).First();
            try {
              foreach (Enumeration subEnumType in Enumeration.GetAll(subType)) {
                // skip excluded subtypes
                if ((subEnumType as IEnumVariableSubType).IsExcluded) {
                  continue;
                }
                if (!tryToBuildAndPopulateTypeMapFor(type, subEnumType)) {
                  remainingTypes.Add(type.FullName, type);
                  noTypesFailedToBuild = false;
                  break;
                }
              }
            } catch (KeyNotFoundException e) {
              Debugger.LogWarning($"Could not build enum {type.FullName} of type {typeof(MappableType)}. Subtype: {subType.FullName}, has probably not been loaded, or needs a dedicated mapper to load. Will try again.\nError: {e}.");
              remainingTypes.Add(type.FullName, type);
              noTypesFailedToBuild = false;
            }
          } else {
            if (!tryToBuildAndPopulateTypeMapFor(type)) {
              remainingTypes.Add(type.FullName, type);
              noTypesFailedToBuild = false;
            }
          }
        }
      }

      typesLeftToConstruct = remainingTypes;
      return noTypesFailedToBuild;
    }

    /// <summary>
    /// Run another mapping attempt
    /// </summary>
    public override void tryToFinishMappingConstructableTypes() {
      if (IsEnabled) {
        if (loadAsComplexEnumTypes(typesLeftToConstruct) || CurrentMappingAttempt == MaxMappingAttemptsCount) {
          afterTypeConstruction();
        }
      }
    }

    /// <summary>
    /// Do something after type construction
    /// </summary>
    protected virtual void afterTypeConstruction() { }

    /// <summary>
    /// Clear all of the data map data
    /// </summary>
    internal override void clearAllDataMaps() {
      CurrentMappingAttempt = 0;
      dataMap = new Dictionary<int, DataMapType>();
      MappableTypeClass = null;
      dataMapConstructor = null;
      genericVariableInterfaceType = null;
      typesLeftToConstruct = new Dictionary<string, Type>();
      uninitalizedAssembliesByType = new Dictionary<string, AssemblyName>();
    }

    /// <summary>
    /// Try to populate a data map for the given type and build the type to make sure it's constants are set up.
    /// </summary>
    internal override bool tryToBuildAndPopulateTypeMapFor(Type typeToConstruct, Enumeration subEnumType = null) {
      Enumeration baseEnumId = subEnumType == null
        ? null
        : typeToConstruct.GetProperty(
          "BaseEnumId",
          BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public
        ).GetValue(null) as Enumeration;

      // get the constructor for building this Enumerati
      ConstructorInfo typeConstructor = typeToConstruct.GetConstructor(
        BindingFlags.Instance | BindingFlags.NonPublic,
        null,
        subEnumType == null
          ? new Type[0]
          : new Type[] { baseEnumId?.Type, subEnumType.GetType() },
        null
      );

      try {
        // try to construct the new type.
        object newType;
        if (subEnumType == null) {
          // simple constructor for simple types
          newType = typeConstructor.Invoke(new object[0]);
        } else {
          // sub-type constructor takes a new enumId made from the baseEnumId and the subtype
          Enumeration subEnumId = Enumeration.MakeSubtype(baseEnumId, subEnumType);
          newType = typeConstructor.Invoke(new object[] { subEnumId, subEnumType });
        }

        // map the newly constructed type to a DataMap object and populate it by pulling data from the streamingassets files.
        MappableType dataObject = (MappableType)(newType);
        dataMap[dataObject.Id] = (DataMapType)dataMapConstructor.Invoke(new object[] { });
        dataMap[dataObject.Id].populate(dataObject);
        Debugger.Log($"Sucessfully mapped data for type: {dataObject.ExternalId}");

        return true;
      } catch (NullReferenceException e) {
        Debugger.LogError($"Will not build class {typeToConstruct.FullName}{(subEnumType == null ? "" : $":{subEnumType.GetType().FullName}")} of type {typeof(MappableType)} as a {MappableTypeClass.FullName}. Could not invoke a private {(subEnumType == null ? "parameterless constructor" : "two element constructor taking the newly created subtype enum Id, and the enumerable value of type " + subEnumType.GetType().FullName)}, or build a DataMap for mapable type class: "
          + typeToConstruct.FullName + $"\n failure: \n" + e.Message + '\n' + e.StackTrace
        );
        return true;
      } catch (System.Reflection.TargetInvocationException i) {
        switch (i.InnerException) {
          case DoNotBuildThisTypeException d:
            Debugger.LogWarning($"Will not build class {typeToConstruct.FullName}{(subEnumType == null ? "" : $":{subEnumType.GetType().FullName}")} of type {typeof(MappableType)} as a {MappableTypeClass.FullName}. DoNotBuildThisTypeException caught in constructor.\n{d}");
            return true;
          // SKIP THESE, WE CANT TRY AGAIN WITH THEM
          case Enumeration.MissingEnumerableTypeDataException me:
            Debugger.LogWarning($"Could not build enum {typeToConstruct.FullName}{(subEnumType == null ? "" : $":{subEnumType.GetType().FullName}")} of type {typeof(MappableType)}. Will try again.\nError: {me.ToString()}.");
            break;
          case Exception x:
            Debugger.LogWarning($"Could not build enum {typeToConstruct.FullName}{(subEnumType == null ? "" : $":{subEnumType.GetType().FullName}")} of type {typeof(MappableType)} due to an Exeption of type {x.GetType().FullName} inside of a reflection invokation. \n{x.Message}\n{x.StackTrace}");
            break;
          default:
            Debugger.LogError($"Could not build enum {typeToConstruct.FullName}{(subEnumType == null ? "" : $":{subEnumType.GetType().FullName}")} of type {typeof(MappableType)} due to an unkown error inside of a reflection invokation.");
            break;
        }
      } catch (Enumeration.MissingEnumerableTypeDataException me) {
        Debugger.LogWarning($"Could not build enum {typeToConstruct.FullName}{(subEnumType == null ? "" : $":{subEnumType.GetType().FullName}")} of type {typeof(MappableType)}. Will try again.\nError: {me.ToString()}.");
      } catch (Exception x) {
        Debugger.LogWarning($"Could not build enum {typeToConstruct.FullName}{(subEnumType == null ? "" : $":{subEnumType.GetType().FullName}")} of type {typeof(MappableType)} due to an Exeption of type {x.GetType().FullName}:\n{x.Message}\n{x.StackTrace}");
      } catch {
        Debugger.LogError($"Could not build enum {typeToConstruct.FullName}{(subEnumType == null ? "" : $":{subEnumType.GetType().FullName}")} of type {typeof(MappableType)} due to an unkown error");
      }

      return false;
    }

    /// <summary>
    /// Register the method used to grab a mapable type from scratch
    /// </summary>
    /// <param name="mapingType"></param>
    void registerMappableTypeFetchMethod(Type mapingType) {
      MethodCallExpression methodRef;
      var methodParam = Expression.Parameter(typeof(string), "id");
      /// grab the right function
      if (!mapingType.IsSubclassOf(typeof(Enumeration))) {
        // on enumerable types it's the Get function of it's connected collection instance variable
        FieldInfo typeField = mapingType.DeclaringType
          .GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static)
          .Where(field => IsSubclassOfRawGeneric(typeof(ArchetypeCollection<,,>), field.FieldType))
          .FirstOrDefault();
        if (typeField == null || string.IsNullOrEmpty(typeField.Name)) {
          throw new MissingFieldException($"Declaring(containing) Class of typeEnum: {mapingType.FullName} does not have a static field of type EnumerableCollection, or may have more than one that can't be used as it's container");
        }

        var typeCollection = typeField.GetValue(null);
        MethodInfo getMethod;
        try {
          getMethod = typeField.FieldType.GetMethod("Get", new Type[] { typeof(string) });
        } catch (Exception e) {
          throw new Exception($"Couldn't get Get function from class {typeField.FieldType.FullName} on field {typeField.Name} for enum type {mapingType.FullName}\n{e.Message}\n{e.StackTrace}");
        }
        Type[] methodParams = getMethod.GetParameters().Select(p => p.ParameterType).ToArray();
        if (methodParams.Length != 1
          || methodParams[0] != typeof(string)
          || !typeof(IMappable).IsAssignableFrom(getMethod.ReturnType)
        ) {
          throw new MissingMethodException($"{mapingType.FullName}: could not find a GET function on the collection for this type that takes only an int.");
        }

        ConstantExpression typeRef = Expression.Constant(typeCollection);
        methodRef = Expression.Call(typeRef, getMethod, methodParam);
      } else {
        // if it's on an Enumeration, then it's the static GetByExternalId function with the Generic for this type
        MethodInfo genericGetMethod = typeof(Enumeration).GetMethod("GetByExternalId").GetGenericMethodDefinition();
        Type[] methodParams = genericGetMethod.GetParameters().Select(p => p.ParameterType).ToArray();
        if (methodParams.Length != 1
          || methodParams[0] != typeof(string)
          || !typeof(IMappable).IsAssignableFrom(genericGetMethod.ReturnType)
        ) {
          throw new MissingMethodException($"{mapingType.FullName}: could not find a GetByExternalId function for this type.");
        }
        MethodInfo getMethod = genericGetMethod.MakeGenericMethod(new Type[] { mapingType });
        methodRef = Expression.Call(null, getMethod, methodParam);
      }

      /// compile the grabbed function
      LambdaExpression fetchFuncBuilder = Expression.Lambda(methodRef, methodParam);
      Delegate fetchExpression = fetchFuncBuilder.Compile();
      Func<string, IMappable> fetchFunc = fetchExpression as Func<string, IMappable>;

      SetMapableFetchFunction(fetchFunc, mapingType);
    }

    /// <summary>
    /// Check if a class is a subclass of the provided generic
    /// </summary>
    static bool IsSubclassOfRawGeneric(Type generic, Type toCheck) {
      while (toCheck != null && toCheck != typeof(object)) {
        var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
        if (generic == cur) {
          return true;
        }
        toCheck = toCheck.BaseType;
      }
      return false;
    }

    #endregion

    #region Mapped Data Access

    /// <summary>
    /// Used to get the whole data map for an item
    /// </summary>
    public bool tryToGetDataMapFor(MappableType type, out DataMapType data) {
      return dataMap.TryGetValue(type.Id, out data);
    }

    /// <summary>
    /// Get a peice of mapped data. Throws exceptions on miss.
    /// </summary>
    protected static object GetMappedData<MapperType>(string dataPropertyName, MappableType type, int? index = null) where MapperType : IArchetypeMapper<MappableType, DataMapType> {
      object[] dataArray;
      Type mapperType = typeof(MapperType);
      // get the map
      return ((IArchetypeMapper<MappableType, DataMapType>)Instances[mapperType.FullName]).tryToGetDataMapFor(type, out DataMapType map)
        // get the field on the map
        ? map.data.TryGetValue(dataPropertyName, out object dataValue) && dataValue != null
          // if the field has an index and it's an array get it by index
          ? index != null
            // make sure it's an array
            ? dataValue.GetType().IsArray
              // make sure the index is in range
              ? ((dataArray = (object[])dataValue)).Length > index
                ? dataArray[(int)index]
                : throw new NoDataMappedForRequestedValueException(
                  Instances[mapperType.FullName],
                  type,
                  dataPropertyName,
                  index
                )
              : throw new ArrayTypeMismatchException($"The data property field {dataPropertyName} on {typeof(DataMapType).FullName} is not an array as expected. Attempted to acces index #{index}")
            : dataValue
          : throw new NoDataMappedForRequestedValueException(
            Instances[mapperType.FullName],
            type,
            dataPropertyName
          )
        : throw new NoDataMappedForRequestedValueException(
          Instances[mapperType.FullName],
          type
        );
    }

    /// <summary>
    /// try to get a peice of mapped data. Does not throw exceptions on miss
    /// </summary>
    protected static bool TryToGetMappedData<MapperType>(string dataPropertyName, MappableType type, out object data, int? index = null) where MapperType : IArchetypeMapper<MappableType, DataMapType> {
      object[] dataArray;
      Type mapperType = typeof(MapperType);
      // get the map
      data = ((IArchetypeMapper<MappableType, DataMapType>)Instances[mapperType.FullName]).tryToGetDataMapFor(type, out DataMapType map)
        // get the field on the map
        ? map.data.TryGetValue(dataPropertyName, out object dataValue)
          // if the field has an index and it's an array get it by index
          ? dataValue != null && index != null
            // make sure it's an array
            ? dataValue.GetType().IsArray
              // make sure the index is in range
              ? ((dataArray = (object[])dataValue)).Length > index
                ? dataArray[(int)index]
                : null
              : throw new ArrayTypeMismatchException($"The data property field {dataPropertyName} on {typeof(DataMapType).FullName} is not an array as expected. Attempted to acces index #{index}")
            // if it's not an array just return it
            : dataValue
          : null
        : null;

      return data != null;
    }

    /// <summary>
    /// Get all the values this data map has for this mappabletype
    /// </summary>
    /// <param name="mappableType"></param>
    /// <returns></returns>
    public override IEnumerable<string> getValuesMappedFor(IMappable mappableType) {
      List<string> initializedValueNames = new List<string>();
      if (mappableType is MappableType) {
        foreach ((string valueName, object value) in dataMap[mappableType.Id].data) {
          if (value != null) {
            initializedValueNames.Add(valueName);
          }
        }
      }

      return initializedValueNames;
    }

    #endregion
  }

  /// <summary>
  /// Base non-abstract class used for mapping archetypes to their collections from dlls
  /// </summary>
  public abstract class ArchetypeMapper : IArchetypeMapper {

    #region Constants

    /// <summary>
    /// The root of all project data. Set via the Initialize() function.
    /// </summary>
    public static string SaveDataRootDirectory {
      get;
      private set;
    }

    /// <summary>
    /// The folder used to store alterable data. Changeable via Initialize()
    /// </summary>
    public const string DataFolder = "data";

    /// <summary>
    /// The current mapping attempt
    /// </summary>
    protected static int CurrentMappingAttempt
      = 0;

    /// <summary>
    /// The most mapping attempts allowed.
    /// This dictates things like recipe complexity depth~
    /// Changeable via Initialize()
    /// </summary>
    public static int MaxMappingAttemptsCount {
      get;
      private set;
    } = 2;

    /// <summary>
    /// This can be used to determine if we're still in the mapping phase of loading data for the game
    /// </summary>
    public static bool MappingInProgress {
      get;
      private set;
    } = true;

    /// <summary>
    /// Pattern for an external library assembly file for this game. Changeable via Initialize()
    /// </summary>
    public const string ProjectAssemblyFilePrefix
      = "SpiritWorlds.";

    /// <summary>
    /// The instances of the data-mapper objects for each type (by fullname of type)
    /// </summary>
    protected static Dictionary<string, IArchetypeMapper> Instances
      = new Dictionary<string, IArchetypeMapper>();

    /// <summary>
    /// How we fetch the value from an int for each mapable data type
    /// </summary>
    static Dictionary<string, Func<string, IMappable>> MappableTypeFetchers {
      get;
    } = new Dictionary<string, Func<string, IMappable>>();

    #region Abstract

    /// <summary>
    /// The mappable type class for this mapper
    /// </summary>
    internal abstract Type MappableTypeClass {
      get;
      set;
    }

    /// <summary>
    /// The name of the folder used to store the data for this type
    /// </summary>
    protected abstract string MapedTypeDataFolderName {
      get;
    }

    /// <summary>
    /// The full save folder root for this data maper's type data
    /// </summary>
    public abstract string DataFolderForThisMappableType {
      get;
    }

    /// <summary>
    /// If the assembly classes we load need to be constructed to register.
    /// This is for EnumerableTypes, not for Enumerations
    /// </summary>
    public virtual bool ConstructAssemblyClasses {
      get;
      private set;
    } = true;

    /// <summary>
    /// The type of data map used by this
    /// </summary>
    public abstract Type DataMapClass {
      get;
    }

    /// <summary>
    /// For testing. Enables and disables this mapper
    /// </summary>
    public virtual bool IsEnabled
      => true;

    #endregion

    #endregion

    /// <summary>
    /// The Constructors for Types that were not able to be initialized in previous mapping runs
    /// </summary>
    public Dictionary<string, AssemblyName> uninitalizedAssembliesByType
      = new Dictionary<string, AssemblyName>();

    /// <summary>
    /// The types we need to construct and map data to
    /// </summary>
    public Dictionary<string, Type> typesLeftToConstruct {
      get;
      internal set;
    }

    /// <summary>
    /// The constructor for this datamap type
    /// </summary>
    protected ConstructorInfo dataMapConstructor;

    #region Initialization

    /// <summary>
    /// Begin the mapping of all types for this mapper
    /// </summary>
    public void startMappingThisArchetype() {
      if (!IsEnabled) {
        return;
      }

      if (ConstructAssemblyClasses) {
        Instances.Add(GetType().FullName, this);
      }
      dataMapConstructor
        = DataMapClass.GetConstructor(
          BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic,
          null,
          new Type[0],
          null
        );
      loadAndMapAllData();
      initialize();
    }

    /// <summary>
    /// initialization
    /// </summary>
    public virtual void initialize() { }

    /// <summary>
    /// Load and map all of the data
    /// </summary>
    internal abstract void loadAndMapAllData();

    /// <summary>
    /// Run a mapping attempt again on the remaining types that need to be constructed
    /// </summary>
    public abstract void tryToFinishMappingConstructableTypes();

    /// <summary>
    /// Try to populate a data map for the given type and build the type to make sure it's constants are set up.
    /// </summary>
    internal abstract bool tryToBuildAndPopulateTypeMapFor(Type typeToConstruct, Enumeration subEnumType = null);

    /// <summary>
    /// Finish all the mapping for all the mappers
    /// </summary>
    public static void FinishConstructingAndMappingAllArchetypes() {
      while (CurrentMappingAttempt < MaxMappingAttemptsCount) {
        CurrentMappingAttempt++;
        foreach (IArchetypeMapper mapper in Instances.Values) {
          if (mapper.ConstructAssemblyClasses && (mapper.typesLeftToConstruct?.Count ?? 0) > 0) {
            mapper.tryToFinishMappingConstructableTypes();
          }
        }
      }

      MappingInProgress = false;
    }

    /// <summary>
    /// Set how to fetch a type by type
    /// </summary>
    protected static void SetMapableFetchFunction(Func<string, IMappable> fetchFunction, Type MappableType) {
      if (typeof(IMappable).IsAssignableFrom(MappableType)) {
        MappableTypeFetchers.Add(MappableType.FullName, fetchFunction);
      } else throw new AccessViolationException($"{MappableType.FullName} does not impliment IMappable");
    }

    #endregion

    /// <summary>
    /// Get a mapable value that was set up by a data mapper
    /// </summary>
    /// <returns></returns>
    public static IMappable GetMapableValue(string externalId, Type MappableType) {
      if (typeof(IMappable).IsAssignableFrom(MappableType)) {
        try {
          return MappableTypeFetchers[MappableType.FullName](externalId);
        } catch (KeyNotFoundException keyNotFound) {
          throw new Exception($"Mapable type with value {externalId} of type {MappableType.FullName} could not be created. There are only DataMapers for the following types:"
            + $"\n Types with mapers: {string.Join(", ", MappableTypeFetchers.Keys)}"
            + $"\n {keyNotFound.Message}"
            + $"\n {keyNotFound.StackTrace}"
          );
        }
      } else throw new AccessViolationException($"{MappableType.FullName} does not impliment IMappable");
    }

    #region Reporting and Testing

    /// <summary>
    /// Clear this mappable type's data
    /// </summary>
    public void clearAllLoadedAndMappedDataForThisType() {
      clearAllDataMaps();
      if (ConstructAssemblyClasses) {
        Archetype.test_utility_ResetArchetypeData(MappableTypeClass);
      }
      Enumeration.Reset(MappableTypeClass);
    }

    /// <summary>
    /// Clear all of the data maps
    /// </summary>
    internal abstract void clearAllDataMaps();

    /// <summary>
    /// For reporting, get what values are mapped to with a value(not null) for this mappabletype
    /// </summary>
    public abstract IEnumerable<string> getValuesMappedFor(IMappable mappableType);

    /// <summary>
    /// Clear all mappers from the instsances list.
    /// For testing only
    /// TODO: Remove before finalizing
    /// </summary>
    public static void ClearInstances() {
      Instances = new Dictionary<string, IArchetypeMapper>();
    }

    #endregion

    /// <summary>
    /// Exception for missing mapper data
    /// </summary>
    public class NoDataMappedForRequestedValueException : KeyNotFoundException {
      public NoDataMappedForRequestedValueException(IArchetypeMapper mapperType, IMappable mappableType, string fieldName = null, int? indexOrMode = null)
        : base(
          $"{mapperType.GetType().Name} is missing data for "
          + (indexOrMode != null
            ? $"index #{indexOrMode} of "
            : "")
          + (!string.IsNullOrEmpty(fieldName)
            ? $"mapped field: '{fieldName}', on"
            : "")
          + $" the mappable type: {mappableType.GetType().FullName}'s enum value: {mappableType} \n"
          + (!string.IsNullOrEmpty(fieldName)
            ? $"Make sure the asset file is not missing for this field"
            : $"Try adding the asset files for this mappable type")
          + $" here: {Path.Combine(mapperType.DataFolderForThisMappableType, mappableType.ExternalId)}\\"
          + (!string.IsNullOrEmpty(fieldName)
            ? $"{fieldName}"
            : "")
          + "\nMapped Values for This type:\n" + string.Join("\n", mapperType.getValuesMappedFor(mappableType))
      ) { }
    }
  }
      
  /// <summary>
  /// Interface for making your own useable mapper
  /// </summary>
  public interface IArchetypeMapper {
    bool ConstructAssemblyClasses {
      get;
    }

    string DataFolderForThisMappableType {
      get;
    }

    Dictionary<string, Type> typesLeftToConstruct {
      get;
    }

    void initialize();
    void startMappingThisArchetype();
    void afterTypeConstruction() { }
    void tryToFinishMappingConstructableTypes();
    IEnumerable<string> getValuesMappedFor(IMappable mappableType);
  }

  /// <summary>
  /// Interface for making your own useable mapper
  /// </summary>
  public interface IArchetypeMapper<MappableType, DataMapType>
    : IArchetypeMapper
    where DataMapType : ArchetypeDataMap<MappableType>
    where MappableType : IMappable 
  {
    bool tryToGetDataMapFor(MappableType type, out DataMapType dataMap);
  }
}
