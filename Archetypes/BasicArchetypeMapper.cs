﻿using System;

namespace Meep.Tech.ModularData.Mapping {

  /// <summary>
  /// An wrapper for the archetype mapper with some basic set-able stats
  /// </summary>
  public class BasicArchetypeMapper<MapableType, DataMapType> 
    : ArchetypeMapper<MapableType, DataMapType> 
    where DataMapType : ArchetypeDataMap<MapableType>
    where MapableType : IMappable 
  {
    protected override string MapedTypeDataFolderName
      => _archetypeDataSaveFolderName;
    readonly string _archetypeDataSaveFolderName;

    protected override Enumeration NoneIdType
      => _emptyTypeEnumId;
    readonly Enumeration _emptyTypeEnumId;

    public override bool ConstructAssemblyClasses
      => _shouldConstructAssemblyClasses;
    readonly bool _shouldConstructAssemblyClasses;

    public override bool IsEnabled
      => _isEnabled;
    private bool _isEnabled
      = true;

    protected override Type getMappableBaseType() {
      return _mappableBaseType ?? base.getMappableBaseType();
    }
    protected System.Type _mappableBaseType;

    public BasicArchetypeMapper(
      string archetypeDataSaveFolderName,
      Enumeration emptyTypeId = null,
      bool shouldConstructAssemblyClasses = true
    ) {
      _emptyTypeEnumId = emptyTypeId;
      _archetypeDataSaveFolderName = archetypeDataSaveFolderName;
      _shouldConstructAssemblyClasses = shouldConstructAssemblyClasses;
    }

    /// <summary>
    /// Dissable this mapper, only works if done before init
    /// </summary>
    public void disable() {
      _isEnabled = false;
    }

    /// <summary>
    /// Set the mappable base type. only works before init
    /// </summary>
    public void setMappableBaseType(Type type) {
      _mappableBaseType = type;
    }

    #region Data Access

    public new static bool TryToGetMappedData<MapperType>(string dataPropertyName, MapableType type, out object data, int? index = null) where MapperType : IArchetypeMapper<MapableType, DataMapType> {
      return ArchetypeMapper<MapableType, DataMapType>.TryToGetMappedData<MapperType>(dataPropertyName, type, out data, index);
    }

    public new static object GetMappedData<MapperType>(string dataPropertyName, MapableType type, int? index = null) where MapperType : IArchetypeMapper<MapableType, DataMapType> {
      return ArchetypeMapper<MapableType, DataMapType>.GetMappedData<MapperType>(dataPropertyName, type, index);
    }

    #endregion
  }
}
