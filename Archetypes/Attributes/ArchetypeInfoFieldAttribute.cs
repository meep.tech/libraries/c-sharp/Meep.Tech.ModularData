﻿using System;

namespace Meep.Tech.ModularData {

  /// <summary>
  /// Represents data that should be considered a recorded/extrnal 'data field' of an attribute
  /// </summary>
  public class ArchetypeInfoFieldAttribute : Attribute {}
}
