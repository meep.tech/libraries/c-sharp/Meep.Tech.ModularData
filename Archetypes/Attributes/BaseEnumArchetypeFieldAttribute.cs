﻿using System;

namespace Meep.Tech.ModularData {

  /// <summary>
  /// Represents a data field of an archetype that comes from it's base id enumeration. Like ID
  /// </summary>
  public class BaseEnumArchetypeFieldAttribute : ArchetypeInfoFieldAttribute {}
}
