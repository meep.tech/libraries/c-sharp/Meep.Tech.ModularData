﻿using System;
using System.Collections.Generic;

namespace Meep.Tech.ModularData {

  /// <summary>
  /// Represents an Archetype that varies based on a specific enumeration
  /// This will register one Item.Type for each enumeration value
  /// </summary>
  /// <typeparam name="SubEnumerationType"></typeparam>
  public interface IEnumVariableArchetype<SubEnumerationType> : IEnumVariableType where SubEnumerationType : Enumeration, IEnumVariableSubType {

    /// <summary>
    /// The type of enum used
    /// This needs to be copy pasted into base types
    /// </summary>
    new Type VariationEnumType {
      get => typeof(SubEnumerationType);
    }
  }

  /// <summary>
  /// Represents an EnumerationType that varies based on a specific enumeration.
  /// Use the above generic type<> to actually enable functionality in the mappers
  /// </summary>
  public interface IEnumVariableType {

    /// <summary>
    /// The id of the enumeration value this variant represents
    /// </summary>
    int VariationEnumId {
      get;
    }

    /// <summary>
    /// The type of enum used
    /// </summary>
    abstract Type VariationEnumType {
      get;
    }

    /// <summary>
    /// The Base enum id value. Needs to be overriden via attribute
    /// </summary>
    static Enumeration BaseEnumId 
      => null;

    /// <summary>
    /// Enum values that are excluded from creation for this type.
    //TODO: replace this with an interface IEnumVariableValueType with a property .IsExcluded;
    /// </summary>
    HashSet<Enumeration> ExcludedEnumValues
      => new HashSet<Enumeration>();
  }

  // Represents a value for the variable enum type one of these variable enum, enum types.
  public interface IEnumVariableSubType {

    /// <summary>
    /// If this is excluded from type creation for the super-enum type(IEnumVariableType<EnumerationType>) for any reason.
    /// </summary>
    bool IsExcluded
      => false;
  }
}