﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
//TODO: remove unity usages:

namespace Meep.Tech.ModularData {

  /// <summary>
  /// An Archetype.
  /// Base to avoid needing generics for accessing enum data
  /// </summary>
  public abstract class Archetype: IMappable, IEquatable<Archetype> {

    #region Data Fields

    #region Basic Data

    /// <summary>
    /// The in-memory id of this type
    /// </summary>
    [BaseEnumArchetypeField]
    public int Id
      => EnumerationIdBase.Id;

    /// <summary>
    /// Name of the type
    /// </summary>
    [BaseEnumArchetypeField]
    public string Name {
      get;
      protected set;
    }

    /// <summary>
    /// the DB unique identifier for this type
    /// </summary>
    [BaseEnumArchetypeField]
    public string ExternalId
      => EnumerationIdBase.ExternalId;

    #endregion

    /// <summary>
    /// The base enum ID, used for archetypes that are modular and variant, and are built from multiple enumerations
    /// </summary>
    [ArchetypeInfoField]
    internal Enumeration EnumerationIdBase {
      get;
      set;
    }

    /// <summary>
    /// Default Params for creating a new empty model, or loading one from the DB. These aren't added automatically in normal Make calls.
    /// </summary>
    [ArchetypeInfoField]
    public virtual Dictionary<Param, object> DefaultEmptyParams
      => null;

    #endregion

    #region Generic Accessors

    /// <summary>
    /// Get it based on it's external id
    /// </summary>
    /// <param name="externalId"></param>
    public abstract IModel MakeEmptyModel(string uniqueID = null);

    /// <summary>
    /// Make and deserialize a model from the given data
    /// </summary>
    internal abstract IModel MakeAndDeserialize(Dictionary<string, object> sqlData, JObject serializedModel);

    #endregion

    #region Equality and Conversion

    /// <summary>
    /// Equality
    /// </summary>
    public bool Equals(Archetype other) {
      return ExternalId == other.ExternalId;
    }

    /// <summary>
    /// hash code
    /// </summary>
    public override int GetHashCode() {
      return ExternalId.GetHashCode();
    }

    public override string ToString() {
      return $"{Id}_/{ExternalId}/";
    }

    #endregion

    #region TESTING

    /// <summary>
    /// Reset this archetype collections to empty
    /// </summary>
    /// <param name="mappableTypeClass"></param>
    public static void test_utility_ResetArchetypeData(Type EnumerableType) {
      ArchetypeCollection.CollectionsByBaseTypeClass.Remove(EnumerableType.FullName, out ArchetypeCollection collection);
      Enumeration.Reset(collection.EnumerationIdType);
    }

    #endregion
  }

  /// <summary>
  /// An enuerable type base class for the Flyweight pattern.
  /// </summary>
  public abstract class Archetype<EnumType, EnumIdType, BaseModelType> : Archetype, IEquatable<Archetype<EnumType, EnumIdType, BaseModelType>>, IEquatable<EnumIdType>
    where EnumType : Archetype<EnumType, EnumIdType, BaseModelType>
    where EnumIdType : Enumeration 
    where BaseModelType : IModel
  {

    /// <summary>
    /// Get the collection containing this archetype
    /// </summary>
    [Newtonsoft.Json.JsonIgnore]
    public ArchetypeCollection<EnumType, EnumIdType, BaseModelType> Collection
      => (ArchetypeCollection<EnumType, EnumIdType, BaseModelType>)ArchetypeCollection.CollectionsByBaseTypeClass[typeof(EnumType).FullName];

    #region Data Fields

    /// <summary>
    /// The base DB mapable id for this type of it's base type
    /// </summary>
    [ArchetypeInfoField]
    public EnumIdType EnumId {
      get;
    }

    #endregion

    #region Initialization

    /// <summary>
    /// Create and Register a new enumerable Type
    /// </summary>
    protected Archetype(EnumIdType typeId, ArchetypeCollection<EnumType, EnumIdType, BaseModelType> containingCollection) {
      EnumerationIdBase = EnumId = typeId;
      Name = EnumId.Name;
      Register(this, containingCollection);
    }

    #region Model Initialization

    /// <summary>
    /// Register
    /// </summary>
    protected internal static void Register(Archetype<EnumType, EnumIdType, BaseModelType> type, ArchetypeCollection<EnumType, EnumIdType, BaseModelType> containingCollection) {
      containingCollection?.Register((EnumType)type);
    }

    /// <summary>
    /// The construction function used to build the model for this type.
    /// </summary>
    /// <returns>The model, along with the index of the next un-used param index</returns>
    protected abstract BaseModelType ConfigureModel(BaseModelType newModel, Dictionary<Param, object> @params);

    /// <summary>
    /// Used to construct the base model for this type
    /// </summary>
    protected internal abstract BaseModelType ModelConstructor(EnumType type);

    /// <summary>
    /// Make a model using this and the rules of the model constructor
    /// </summary>
    protected BaseModelType Make(Dictionary<Param, object> @params) {
      try {
        BaseModelType newModel = ModelConstructor((EnumType)this);
        return ConfigureModel(newModel, @params ?? new Dictionary<Param, object>());
      } catch (InvalidCastException i) {
        throw new InvalidCastException($"ConfigureModel for {GetType()} could not cast the base model to the required model extension type. Did you forget to set ModelConstructor to this new extension type?\n{i.ToString()}");
      } catch (MissingParamException mp) {
        throw new MissingParamException($"Exception thrown while trying to construct a model for type: {this}. Exeption:\n{mp.ToString()}");
      } catch (ParamMissmatchException pm) {
        throw new ParamMissmatchException($"Exception thrown while trying to construct a {typeof(BaseModelType).Name} model for EnumType: {this}. Exeption:\n{pm.ToString()}");
      }
    }
    
    /// <summary>
    /// try to build a model from serialized sql and json data
    /// </summary>
    public static BaseModelType Make(Dictionary<string, object> sqlData, JObject jsonData) {
      // Error handling, items need an id and type field to be deserialized
      string externalTypeId = null;
      string uniqueModelId = null;
      try {
        if (sqlData.TryGetValue(ModelSerializer.SQLIDColumnName, out object id)) {
          uniqueModelId = id as string;
        }
        externalTypeId = sqlData[ModelSerializer.SQLTypeColumnName] as string;
      } catch (Exception e){
        throw new Exception($"Could not deserialize model ID:{uniqueModelId ?? "NO ID"}'s data for type {externalTypeId ?? "ERROR NO TYPE FOUND"}\n{sqlData.Select(entry => $"{entry.Key}:{entry.Value}").Join("\n") ?? "ERROR NO SQL DATA FOUND"}\n{e}");
      }
      if (externalTypeId == null) {
        throw new Exception($"Could not deserialize model ID:{uniqueModelId ?? "NO ID"}'s data for type {externalTypeId ?? "ERROR NO TYPE FOUND"}\n{sqlData.Select(entry => $"{entry.Key}:{entry.Value}").Join("\n") ?? "ERROR NO SQL DATA FOUND"}");
      }

      /// Make an empty model of the given type
      Archetype type
       = ArchetypeCollection.CollectionsByBaseTypeClass[typeof(EnumType).FullName]
        .GetFromExternal(externalTypeId);
      IModel model = type.MakeEmptyModel(uniqueModelId);

      return (BaseModelType)ModelSerializer.Deserialize(model, sqlData, jsonData);
    }

    /// <summary>
    /// try to build a model from serialized sql and json data
    /// </summary>
    public static ModelType Make<ModelType>(Dictionary<string, object> sqlData, JObject serializedModel) where ModelType : BaseModelType {
      return (ModelType)Make(sqlData, serializedModel);
    }

    /// <summary>
    /// Make an empty model
    /// </summary>
    /// <returns></returns>
    public override IModel MakeEmptyModel(string uniqueId = null) {
      var @params = uniqueId != null
        ? new Dictionary<Param, object>() { { Param.UniqueId, uniqueId } }
        : null;
      // include any default params
      if (DefaultEmptyParams != null) {
        @params = @params == null
           ? DefaultEmptyParams
           : @params.Merge(DefaultEmptyParams);
      }
      return Make(@params);
    }

    /// <summary>
    /// Internal Generic version for making a model from serialized data
    /// </summary>
    internal override IModel MakeAndDeserialize(Dictionary<string, object> sqlData, JObject serializedModel) {
      return Make(sqlData, serializedModel);
    }

    /// <summary>
    /// Load a model from DB by ID.
    /// Does not work for Simple models
    /// </summary>
    /// <returns></returns>
    public BaseModelType Load(string uniqueItemId, Dictionary<Param, object> initialParams = null) {
      IUniqueModel model = Make(new Dictionary<Param, object>() {
        {Param.UniqueId, uniqueItemId }
      }.Merge(DefaultEmptyParams).Merge(initialParams)) as IUniqueModel;
      return (BaseModelType)ModelSerializer.Load(model);
    }

    #endregion

    #endregion

    #region Equality and Conversion

    /// <summary>
    /// hash code
    /// </summary>
    public override int GetHashCode() {
      return Id;
    }

    /// <summary>
    /// equality with id
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public bool Equals(EnumIdType other) {
      return Id == other.Id;
    }

    /// <summary>
    /// Equality
    /// </summary>
    public bool Equals(Archetype<EnumType, EnumIdType, BaseModelType> other) {
      return Id == other.Id;
    }

    #endregion
  }
}
