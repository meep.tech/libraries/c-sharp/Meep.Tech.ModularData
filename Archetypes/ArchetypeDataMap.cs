﻿

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Meep.Tech.ModularData.Mapping {

  /// <summary>
  /// A map of asset data to an archetype.
  /// </summary>
  public abstract class ArchetypeDataMap<MapableType> where MapableType : IMappable {

    #region Constants

    #region Abstract

    /// <summary>
    /// The name of the folder used to store the data for this type
    /// Should match the parent maper's folder name
    /// </summary>
    protected abstract string ModelFolder {
      get;
    }

    /// <summary>
    /// The data properties
    /// </summary>
    protected virtual string[] DataProperties
      => new string[0];

    /// <summary>
    /// functions used to map each data to the correct type
    /// </summary>
    protected virtual Dictionary<string, Func<string, object>> MapFunctions
      => new Dictionary<string, Func<string, object>>();

    /// <summary>
    /// If this types datamap is mapped from one single unity asset, use this function.
    /// Also make sure to set the MapObjectName is set
    /// </summary>
    protected virtual Func<string, object> SingleMapFunction
      => null;

    /// <summary>
    /// The name of the single object used to create the entire map from SingleMapFunction
    /// </summary>
    protected virtual string MapObjectName {
      get;
    } = null;

    #endregion

    #endregion

    /// <summary>
    /// The mapped data
    /// </summary>
    internal Dictionary<string, object> data
      = new Dictionary<string, object>();

    /// <summary>
    /// Returns the name of the folder this data map uses to store data for this model
    /// </summary>
    /// <returns></returns>
    public virtual string getFolderNameToUseForMapableType(MapableType type) {
      return type.ExternalId.ToLower();
    }

    /// <summary>
    /// Try to set a data field
    /// </summary>
    protected void set(string dataPropertyFieldName, object value) {
      if (DataProperties.Contains(dataPropertyFieldName)) {
        data[dataPropertyFieldName] = value;
      } else throw new KeyNotFoundException($"Trying to set non-existent dataProperty field on DataMap of type {GetType()}. This map does not have a DataProperties field named: {dataPropertyFieldName}");
    }

    /// <summary>
    /// Populate the data map for the given item type
    /// </summary>
    internal void populate(MapableType type) {
      string typeFolder = Path.Combine(ArchetypeMapper.SaveDataRootDirectory, ArchetypeMapper.DataFolder, ModelFolder, getFolderNameToUseForMapableType(type));
      if (SingleMapFunction != null) {
        data[MapObjectName] = SingleMapFunction(Path.Combine(typeFolder, MapObjectName));
      }
      foreach (string dataProperty in DataProperties) {
        Func<string, object> mapFunction = MapFunctions[dataProperty];
        data[dataProperty] = mapFunction(Path.Combine(typeFolder, dataProperty));
      }
    }
  }
}