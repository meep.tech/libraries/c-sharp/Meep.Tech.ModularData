﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Meep.Tech.ModularData {

  /// <summary>
  /// Simple base for generics for EnumerableCollections
  /// </summary>
  public abstract class ArchetypeCollection {

    /// <summary>
    /// All of the EnumerableCollections of EnumerableTypes
    /// TODO: remove?
    /// </summary>
    public static Dictionary<string, ArchetypeCollection> CollectionsByBaseTypeClass
      = new Dictionary<string, ArchetypeCollection>();

    /// <summary>
    /// All types
    /// </summary>
    public abstract IEnumerable<Archetype> Enums {
      get;
    }

    /// <summary>
    /// The type of enum for this
    /// </summary>
    internal abstract Type EnumerationIdType {
      get;
    }

    /// <summary>
    /// Get it based on it's external id
    /// </summary>
    /// <param name="externalId"></param>
    internal protected abstract Archetype GetFromExternal(string externalId);

    /// <summary>
    /// Get it based on it's external id
    /// </summary>
    /// <param name="externalId"></param>
    internal protected abstract Archetype GetFromInternal(int internalId);

    /// <summary>
    /// Make and deserialize a model from the given data
    /// </summary>
    internal abstract IModel MakeAndDeserialize(Dictionary<string, object> sqlData, JObject serializedJSON);

    /// <summary>
    /// Get a random type for the given model type
    /// </summary>
    /// <param name="modelType"></param>
    /// <returns></returns>
    internal abstract Archetype GetRandomTypeForModel(Type modelType);
  }

  /// <summary>
  /// A collection of the enumerable type.
  /// </summary>
  public class ArchetypeCollection<EnumType, EnumIdType, BaseModelType> : ArchetypeCollection, IEnumerable<EnumType>
    where EnumType : Archetype<EnumType, EnumIdType, BaseModelType> 
    where EnumIdType : Enumeration 
    where BaseModelType : IModel
  {

    /// <summary>
    /// All types
    /// </summary>
    public IEnumerable<EnumType> All
      => all.Values;

    /// <summary>
    /// All types
    /// </summary>
    public override IEnumerable<Archetype> Enums
      => All;

    /// <summary>
    /// All types
    /// </summary>
    internal override Type EnumerationIdType
      => typeof(EnumIdType);

    /// <summary>
    /// Empty type for this collection
    /// </summary>
    public EnumType None {
      get;
      private set;
    } = null;

    /// <summary>
    /// All item types registered
    /// </summary>
    protected Dictionary<int, EnumType> all
      = new Dictionary<int, EnumType>();

    /// <summary>
    /// The arechetypes stored by the type of model they produce
    /// </summary>
    Dictionary<string, HashSet<int>> archetypesByModelType
      = new Dictionary<string, HashSet<int>>();

    #region Initialization

    /// <summary>
    /// Make a new collection of enumtypes
    /// </summary>
    public ArchetypeCollection(Type noneEnumType) {
      if (typeof(Archetype<EnumType, EnumIdType, BaseModelType>).IsAssignableFrom(noneEnumType)) {
        try {
          Register((EnumType)(
            noneEnumType
              .GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[0], null)
              .Invoke(new object[0])
          ));
        } catch (Exception e) {
          throw new MissingMemberException($"Could not register type: {noneEnumType.FullName}. It may be missing a private paramaterless constructor. \n {e.Message} \n {e.StackTrace}");
        }
        CollectionsByBaseTypeClass.Add(typeof(EnumType).FullName, this);
      } else throw new TypeAccessException($"Cannot make new EnumerableCollection. {noneEnumType.FullName} does not inheret from {typeof(Archetype<EnumType, EnumIdType, BaseModelType>).FullName}");
    }

    /// <summary>
    /// Register this type on collection
    /// </summary>
    internal void Register(EnumType type) {
      if (!all.TryGetValue(type.Id, out EnumType existingType)) {
        all.Add(type.Id, type);
      } else if (existingType.ExternalId != type.ExternalId) {
        throw new System.Exception($"Cannot register {type.Name} as a new {typeof(EnumType).FullName}; Item with id:{type.Id} exists. Registered as item: {existingType.Name}");
      } else {
        all[type.Id] = type;
      }

      // add it to the collection of archetypes by model type:
      string archetypeModelType = type.ModelConstructor(type)?.GetType().FullName ?? "null";
      if (archetypesByModelType.TryGetValue(archetypeModelType, out HashSet<int> currentIds)) {
        if (!currentIds.Contains(type.Id)) {
          currentIds.Add(type.Id);
        }
      } else {
        archetypesByModelType.Add(archetypeModelType, new HashSet<int> { type.Id });
      }
    }

    /// <summary>
    /// Deserialize and make a model for one of the types this Archetype collection holds
    /// </summary>
    internal override IModel MakeAndDeserialize(Dictionary<string, object> sqlData, JObject serializedModel) {
      return GetFromExternal(sqlData[ModelSerializer.SQLTypeColumnName] as string).MakeAndDeserialize(sqlData, serializedModel);
    }

    #endregion

    /// <summary>
    /// Get a type by id
    /// </summary>
    public EnumType Get(int typeId) {
      return all.TryGetValue(typeId, out EnumType type)
        ? type
        : throw new Enumeration.MissingEnumerableTypeDataException(typeId, all.Values.Select(type => type.ToString()), typeof(EnumType).FullName);
    }

    /// <summary>
    /// Get a type by external id
    /// </summary>
    public EnumType Get(string externalId) {
      return all.TryGetValue(Enumeration.GetByExternalId<EnumIdType>(externalId).Id, out EnumType type)
        ? type
        : throw new Enumeration.MissingEnumerableTypeDataException(externalId, all.Values.Select(type => type.ToString()), typeof(EnumType).FullName);
    }

    /// <summary>
    /// Get a type by id
    /// </summary>
    public EnumType Get(EnumIdType typeId) {
      return all.TryGetValue(typeId.Id, out EnumType type)
        ? type
        : throw new Enumeration.MissingEnumerableTypeDataException(typeId, all.Values.Select(type => type.ToString()), typeof(EnumType).FullName);
    }

    /// <summary>
    /// Get a type by external id
    /// </summary>
    internal protected override Archetype GetFromExternal(string externalId) {
      return Get(externalId);
    }

    /// <summary>
    /// Get a type by external id
    /// </summary>
    internal protected override Archetype GetFromInternal(int internalId) {
      return Get(internalId);
    }

    /// <summary>
    /// Get a random archetype for the given model type
    /// TODO: fix this so if it's a generic base type it'll still match
    /// </summary>
    internal override Archetype GetRandomTypeForModel(Type modelType)
      => archetypesByModelType.TryGetValue(modelType.FullName, out HashSet<int> values)
          ? Get(values.ToList().Shuffle().First())
          : null;

    #region IEnumerable

    /// <summary>
    /// enumerate over all types
    /// </summary>
    /// <returns></returns>
    public IEnumerator<EnumType> GetEnumerator() {
      return All.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator() {
      return GetEnumerator();
    }

    #endregion
  }
}
