﻿namespace Meep.Tech.ModularData {

  /// <summary>
  /// indicates an object or model with a type and unique ID.
  /// Unique items are also expected to have some type interface
  /// </summary>
  public interface IUnique {

    /// <summary>
    /// the UUID of the object
    /// </summary>
    string id {
      get;
    }
  }
}