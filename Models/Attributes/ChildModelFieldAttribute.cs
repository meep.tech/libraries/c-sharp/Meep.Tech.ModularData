﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Meep.Tech.ModularData {

  /// <summary>
  /// Represents a field with different serialization and deserialization depending on light loading vs full loading, as it's a field that contains other models.
  /// </summary>
  public class ChildModelFieldAttribute : CustomModelDataFieldAttribute {

    /// <summary>
    /// If we want to prevent this child field from ever light-loading
    /// </summary>
    public bool IsUnique {
      get;
    } = false;

    /// <summary>
    /// Model Type
    /// </summary>
    public Type ModelType {
      get;
    }

    /// <summary>
    /// The type this serializes to
    /// </summary>
    public override Type CustomSerializeToType 
      => ModelType;

    /// <summary>
    /// If we shouldn't try to set the child model data to the field value after deserializing.
    /// This is helpful if you used advanced deserialization to set the child model onto the parent already
    /// </summary>
    public bool SkipSettingValueToFieldAfterDeserialzation
      => skipSettingValueToFieldAfterDeserialzation || HasAdvancedChildDeserialization;
    bool skipSettingValueToFieldAfterDeserialzation
      = false;

    /// <summary>
    /// If this uses advanced child deserialization.
    /// This will make it so we skip setting the value to the model, as we assume advanced deserialization does this for us
    /// </summary>
    public virtual bool HasAdvancedChildDeserialization
      => ChildModelAdvancedDeserializationFunction != null;

    /// <summary>
    /// If this attribute uses advanced child serialization
    /// </summary>
    public bool HasAdvancedChildSerialization
      => ChildModelAdvancedSerializationFunction != null;

    /// <summary>
    /// Table name for the model type
    /// </summary>
    public string TableName
      => ModelSerializer.Cache.GetDataModelInfo(ModelType).TableName;

    /// <summary>
    /// Helper for making the child function match the returns you need.
    /// </summary>
    public virtual Func<object, ModelSerializer.SerializedModelDataCollection> ChildModelSerializationFunction
      => modelFieldValue => ModelSerializer.Serialize((IModel)modelFieldValue);

    /// <summary>
    /// Default for full deserialization
    /// </summary>
    public virtual Func<string, string, ModelSerializer.SerializedData, IModel> ChildModelDeserializationFunction
      => (uniqueModelId, modelTypeExternalId, serializedModel) => {
        IModel childModel = GetEmptyModel(modelTypeExternalId);
        return ModelSerializer.Deserialize(childModel, serializedModel.sqlData, serializedModel.jsonData);
      };

    /// <summary>
    /// Default for full deserialization
    /// </summary>
    public virtual Func<string, string, ModelSerializer.SerializedData, IUniqueModel, IModel> ChildModelAdvancedDeserializationFunction
      => null;

    /// <summary>
    /// An advanced serialization option for child models that includes the parent
    /// </summary>
    public virtual Func<object, IModel, ModelSerializer.SerializedModelDataCollection> ChildModelAdvancedSerializationFunction
      => null;

    /// <summary>
    /// Overrideable function for getting the empty model
    /// </summary>
    public virtual Func<string, IModel> GetEmptyModel 
      => externalTypeId => typeof(Archetype).IsAssignableFrom(ModelType) 
        ? ArchetypeCollection.CollectionsByBaseTypeClass[ModelType.FullName].GetFromExternal(externalTypeId).MakeEmptyModel()
        : (IModel)Activator.CreateInstance(ModelType);

    /// <summary>
    /// Serialization override to point to the ChildModelSerializationFunction.
    /// Override that in children instead
    /// </summary>
    public override Func<object, object> SerializationFunction
      => ChildModelSerializationFunction;

    internal protected override Func<JToken, object> DeserializeJSON
      => (serializedData) => throw new NotImplementedException("You cannot normally de-serialize child model fields");

    internal protected override Func<object, object> DeserializeObject
      => (serializedData) => throw new NotImplementedException("You cannot normally de-serialize child model fields");

    /// <summary>
    /// Make a child field with a single model
    /// </summary>
    protected ChildModelFieldAttribute(
      Type ModelType,
      string FieldName = null,
      bool SkipSettingValueToFieldAfterDeserialzation = false,
      bool SkipDuringDeserialization = false, 
      bool skipOwnerCheck = false
    ) : base(FieldName, SkipDuringDeserialization) {
      this.ModelType = ModelType;
      IsChildModelField = true;
      skipSettingValueToFieldAfterDeserialzation = SkipSettingValueToFieldAfterDeserialzation;
      IsUnique = typeof(IUniqueModel).IsAssignableFrom(ModelType);
      if (!skipOwnerCheck && !typeof(IOwned).IsAssignableFrom(ModelType)) {
        throw new NotImplementedException($"Cannot set a model as a child model type unless it impliments IOwned. {ModelType?.ToString() ?? "null"} does not impliment IOwned");
      }
    }

    /// <summary>
    /// Make a child field with a single model
    /// </summary>
    public ChildModelFieldAttribute(Type ModelType, string FieldName = null, bool SkipSettingValueToFieldAfterDeserialzation = false, bool SkipDuringDeserialization = false) 
      : this(ModelType, FieldName, SkipSettingValueToFieldAfterDeserialzation, SkipDuringDeserialization, false) { }
  }
}
