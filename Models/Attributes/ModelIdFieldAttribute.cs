﻿namespace Meep.Tech.ModularData {
  /// <summary>
  /// Used to represent the unique ID of a model.
  /// </summary>
  public class ModelIdFieldAttribute : ModelDataFieldAttribute {
    public ModelIdFieldAttribute() : base(ModelSerializer.SQLIDColumnName, IsASQLField: true) {}
  }
}
