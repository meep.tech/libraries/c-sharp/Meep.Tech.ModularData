﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Meep.Tech.ModularData {
  /// <summary>
  /// Default for advanced deserialization methods, in case you need the parent model data to fetch from cache
  /// </summary>
  public class AdvancedLinkedModelDataField : LinkedModelDataFieldAttribute {

    /// <summary>
    /// Advanced default. Make sure to override this to actually set the value on the parent model. This is JUST A HELPER to fetch it from cache
    /// </summary>
    public override Func<string, string, ModelSerializer.SerializedData, IUniqueModel, IModel> ChildModelAdvancedDeserializationFunction 
      => (uniqueModelId, modelTypeExternalId, serializedModel, parentModel) => {
        IModel baseChildModel;
        if ((baseChildModel = FetchModelFromCache?.Invoke(uniqueModelId, modelTypeExternalId, serializedModel.jsonData, parentModel)) != null) {
          return baseChildModel;
        } else return ModelSerializer.Deserialize(GetEmptyModel(modelTypeExternalId), serializedModel.sqlData, serializedModel.jsonData);
      };

    public AdvancedLinkedModelDataField(
      Type ModelType, 
      string FieldName = null,
      bool SkipSettingValueToFieldAfterDeserialzation = false, 
      bool SkipDuringDeserialization = false
    ) : base(ModelType, FieldName, SkipSettingValueToFieldAfterDeserialzation, SkipDuringDeserialization) {}
  }
}
