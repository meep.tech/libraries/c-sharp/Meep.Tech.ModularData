﻿namespace Meep.Tech.ModularData {

  /// <summary>
  /// Used to represent the unique ID of the owner/parent model of this child model.
  /// </summary>
  public class ModelOwnerFieldAttribute : ModelDataFieldAttribute {
    public ModelOwnerFieldAttribute(bool SkipDuringDeserialization = false) : base(ModelSerializer.SQLOwnerColumnName, IsASQLField: true, SkipDuringDeserialization) { }
  }
}
