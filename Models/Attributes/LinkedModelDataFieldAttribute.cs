﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Meep.Tech.ModularData {

  /// <summary>
  /// A linked model, one that's not owned but is saved in it's own table under it's own UUID.
  /// By default this will load the model as a new model with the desired data from the DB. If you want this to find an existing cached model by id you need to overload it.
  /// </summary>
  public class LinkedModelDataFieldAttribute : ChildModelFieldAttribute {

    /// <summary>
    /// Overrideable function to search a cache for the item based on:
    /// it's uniqueId, archetype externalId, json data, and (if using advances deserialization) the parent model
    /// </summary>
    public virtual Func<string, string, JObject, IModel, IModel> FetchModelFromCache
      => null;

    /// <summary>
    /// Default for full deserialization
    /// </summary>
    public override Func<string, string, ModelSerializer.SerializedData, IModel> ChildModelDeserializationFunction
      => (uniqueModelId, modelTypeExternalId, serializedModel) => {
        IModel childModel;
        // try to fetch from cache
        if ((childModel = FetchModelFromCache?.Invoke(uniqueModelId, modelTypeExternalId, serializedModel.jsonData, null)) != null) {
          return childModel;
        } else return base.ChildModelDeserializationFunction(uniqueModelId, modelTypeExternalId, serializedModel);
      };

    public LinkedModelDataFieldAttribute(
      Type ModelType,
      string FieldName = null,
      bool SkipSettingValueToFieldAfterDeserialzation = false,
      bool SkipDuringDeserialization = false
    ) : base(
      ModelType,
      FieldName,
      SkipSettingValueToFieldAfterDeserialzation,
      SkipDuringDeserialization,
      true
    ) {
      if (!IsUnique) {
        throw new Exception($"LinkedModelDataFieldAttribute must be a IUnique model type so we can find it via ID");
      }
    }
  }
}
