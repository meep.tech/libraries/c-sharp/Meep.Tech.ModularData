﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Meep.Tech.ModularData {

  /// <summary>
  /// Used to serialize and deserialize a collection(IEnumeration) of child models in one field
  /// </summary>
  public class ChildModelsCollectionFieldAttribute : ChildModelFieldAttribute {

    /// <summary>
    /// If the serializer should delete all children of this type from the DB for this model before saving them again
    /// </summary>
    public bool DeleteAllOldChildrenBeforeSavingNewChildrenOfThisType {
      get;
    } = false;

    /// <summary>
    /// Base type as an enum
    /// </summary>
    public override Type CustomSerializeToType 
      => typeof(IEnumerable<>).MakeGenericType(base.CustomSerializeToType);

    /// <summary>
    /// Serialize the entire enumerable as opposed to one model at a time:
    /// </summary>
    public override Func<object, ModelSerializer.SerializedModelDataCollection> ChildModelSerializationFunction 
      => modelList => {
        ModelSerializer.SerializedModelDataCollection serializedObjects 
          = new ModelSerializer.SerializedModelDataCollection();
        // if it's a dictonary, by default we assume the values are the models
        if (modelList is IDictionary modelDictionary) {
          modelList = modelDictionary.Values;
        }
        foreach (object model in modelList as System.Collections.IEnumerable) {
          serializedObjects.Merge(SingleChildModelSerializationFunction(model));
        }

        return serializedObjects;
      };

    /// <summary>
    /// Accessor for the single child model serialization function
    /// </summary>
    public Func<object, ModelSerializer.SerializedModelDataCollection> SingleChildModelSerializationFunction
      => model => base.ChildModelSerializationFunction(model);

    public ChildModelsCollectionFieldAttribute(Type ModelType, string FieldName = null, bool SkipDuringDeserialization = false, bool SkipSettingValueToFieldAfterDeserialzation = false) 
      : base(ModelType, FieldName, SkipSettingValueToFieldAfterDeserialzation, SkipDuringDeserialization) { }

    protected ChildModelsCollectionFieldAttribute(string FieldName, Type ModelType, bool SkipOwnerCheck = false, bool SkipSettingValueToFieldAfterDeserialzation = false, bool SkipDuringDeserialization = false, bool DeleteAllOldChildrenBeforeSavingNewChildrenOfThisType = false) 
      : base(
          ModelType,
          FieldName,
          SkipSettingValueToFieldAfterDeserialzation,
          SkipDuringDeserialization,
          SkipOwnerCheck
    ) {
      this.DeleteAllOldChildrenBeforeSavingNewChildrenOfThisType = DeleteAllOldChildrenBeforeSavingNewChildrenOfThisType;
    }
  }
}
