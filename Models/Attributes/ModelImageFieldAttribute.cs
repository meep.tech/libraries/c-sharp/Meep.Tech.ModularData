﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Meep.Tech.ModularData {

  /// <summary>
  /// Used to save an image as a field of a model as bytes to the DB.
  /// These fields are always saved unless they have a dirty field that's not maked dirty, and are loaded as long as children are loaded.
  /// </summary>
  public abstract class ModelImageFieldAttribute : ChildModelFieldAttribute {

    /// <summary>
    /// Supported image types
    /// </summary>
    public enum ImageType {
      PNG
    }

    #region Constants

    /// <summary>
    /// Images table name
    /// </summary>
    public const string ImageTableName = "Images";

    /// <summary>
    /// Images table owner model table name
    /// </summary>
    public const string ImageOwnerTypeColumnName = "ownerModelType";

    /// <summary>
    /// Images table owner model table name
    /// </summary>
    public const string ImageOwnerFieldColumnName = "ownerModelField";

    /// <summary>
    /// Images table owner model table name
    /// </summary>
    public const string ImageOwnerColumnName = "owner";

    /// <summary>
    /// Images table name
    /// </summary>
    public const string ImageBytesColumnName = "bytes";

    /// <summary>
    /// Table columns for the image table
    /// </summary>
    public static Dictionary<string, Type> ImageTableColumns {
      get;
    } = new Dictionary<string, Type> {
      {
        ImageOwnerColumnName,
        typeof(string)
      },
      {
        ImageOwnerTypeColumnName,
        typeof(string)
      },
      {
        ImageOwnerFieldColumnName,
        typeof(string)
      },
      {
        "filename",
        typeof(string)
      },
      {
        ImageBytesColumnName,
        typeof(byte[])
      },
    };

    #endregion

    /// <summary>
    /// Encode the image to bytes
    /// </summary>
    internal protected abstract byte[] EncodeToBytes(object image);

    /// <summary>
    /// Decode image bytes to an image object of the fields required type
    /// </summary>
    internal protected abstract object DeserializeFromBytes(byte[] imageBytes);

    /// <summary>
    /// Encode the image to bytes
    /// </summary>
    internal protected virtual string GetFileName(string modelName, string fieldName, object image)
      => $"{modelName}_{fieldName}.{imageType}";

    /// <summary>
    /// If the field should be saved because it's been marked dirty.
    /// </summary>
    public Func<object, bool> CheckisDirty {
      get;
    }

    /// <summary>
    /// Type of image to save as
    /// </summary>
    protected virtual ImageType imageType
      => ImageType.PNG;

    /// <summary>
    /// Turn this image into a saveable row of sql for the image table
    /// </summary>
    internal protected virtual Dictionary<string, object> SerializeToSQLRow(string modelId, string fieldName, string modelName, object image) {
      Dictionary<string, object> queryParams = new Dictionary<string, object> {
        {
          ImageOwnerColumnName,
          modelId
        },
        {
          ImageOwnerTypeColumnName,
          modelName
        },
        {
          ImageOwnerFieldColumnName,
          fieldName
        },
        {
          "filename",
          GetFileName(modelName, fieldName, image)
        },
        {
          ImageBytesColumnName,
          EncodeToBytes(image)
        },
      };

      return queryParams;
    }

    /// <summary>
    /// Set up the model field attribute, and if it has a dirty field check
    /// </summary>
    /// <param name="DirtyFlagBooleanFieldName"></param>
    protected ModelImageFieldAttribute(string DirtyFlagBooleanFieldName = null, string FieldName = null) : base(typeof(byte[]), FieldName, true, false, true) {
      if (DirtyFlagBooleanFieldName != null) {
        // if we were provided with a boolean field name, use it for the dirty check when saving
        CheckisDirty = new Func<object, bool>(parentModel => {
          FieldInfo field =
            parentModel.GetType()
              .GetField(
                DirtyFlagBooleanFieldName,
                System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public
              );
          if (field != null) {
            // if it's dirty, we're about to save it, so set it clean again
            if ((bool)field.GetValue(parentModel)) {
              field.SetValue(parentModel, false);

              return true;
            } else {
              return false;
            }
          } else throw new MissingFieldException($"No boolean field named: {DirtyFlagBooleanFieldName}, found on model of type: {parentModel.GetType()}");
        });
      }
    }
  }
}
