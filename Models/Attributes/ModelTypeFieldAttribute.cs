﻿using System;

namespace Meep.Tech.ModularData {

  /// <summary>
  /// Used to indicate the Type (Archetype) field on a model.
  /// This can be either an Enum, Enumeration, or Archetype
  /// </summary>
  public class ModelTypeFieldAttribute : ModelDataFieldAttribute {

    /// <summary>
    /// Base type of this EumerableType field. 
    /// </summary>
    Type ArchetypeBaseType { get; }

    public override bool HasCustomSerialization 
      => true;

    public override bool HasCustomDeserialization 
      => true;

    public override Type CustomSerializeToType
      => typeof(string);

    /// <summary>
    /// Make using the field type
    /// </summary>
    public ModelTypeFieldAttribute(
      Type ArchetypeBaseType
    ) : base(ModelSerializer.SQLTypeColumnName, IsASQLField: true, SkipDuringDeserialization: true) {
      if (typeof(Enum).IsAssignableFrom(ArchetypeBaseType)
        || typeof(Enumeration).IsAssignableFrom(ArchetypeBaseType)
        || typeof(Archetype).IsAssignableFrom(ArchetypeBaseType)
      ) {
        this.ArchetypeBaseType = ArchetypeBaseType;
      } else throw new NotImplementedException($"Cannot use {ArchetypeBaseType} as an Archetype for models");
    }

    /// <summary>
    /// Set the value with the external id
    /// </summary>
    public override Func<object, object> SerializationFunction
      => type
        => type is IMappable mappable
          ? mappable.ExternalId
          : type.ToString();

    /// <summary>
    /// Get the enum from an external id
    /// </summary>
    internal protected override Func<object, object> DeserializeObject
      => externalTypeId => typeof(Enumeration).IsAssignableFrom(ArchetypeBaseType)
        ? (object)Enumeration.GetByExternalId(externalTypeId as string, ArchetypeBaseType)
        : typeof(Enum).IsAssignableFrom(ArchetypeBaseType)
          ? Enum.Parse(ArchetypeBaseType, externalTypeId as string)
          : ArchetypeCollection.CollectionsByBaseTypeClass[ArchetypeBaseType.FullName]
            .GetFromExternal(externalTypeId as string);
  }
}
