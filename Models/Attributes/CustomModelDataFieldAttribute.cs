﻿namespace Meep.Tech.ModularData {

  /// <summary>
  /// Attribute used to dictate a field on a model we should serialize.
  /// Make a new one with the functionality inside of it
  /// </summary>
  public abstract class CustomModelDataFieldAttribute : ModelDataFieldAttribute {

    /// <summary>
    /// If this has custom serialization
    /// </summary>
    public override bool HasCustomSerialization
      => SerializationFunction != null;

    /// <summary>
    /// If this has custom serialization
    /// </summary>
    public override bool HasCustomDeserialization
      => (DeserializeObject ?? DeserializeJSON) != null;

    /// <summary>
    /// If this has custom serialization
    /// </summary>
    public override bool HasAdvancedJSONFieldDeserialization
      => AdvancedJSONDeserializationFunction != null;

    //base ctor for making new custom fields:
    protected CustomModelDataFieldAttribute(string FieldName = null, bool SkipDuringDeserialization = false, bool IsASqlField = false) 
      : base(FieldName, IsASqlField, SkipDuringDeserialization) {}
  }
}
