﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Meep.Tech.ModularData {

  /// <summary>
  /// Represents a model that can be saved to json
  /// </summary>
  [AttributeUsage(AttributeTargets.Class |AttributeTargets.Struct, Inherited = true)]
  public class DataModelAttribute : Attribute {

    /// <summary>
    /// If this data model has a tupe field. Defaults to true
    /// </summary>
    public bool HasTypeField {
      get;
    }

    /// <summary>
    /// The base type of the achretype used by this model type
    /// </summary>
    public Type ArchetypeBaseType {
      get;
    } = null;

    /// <summary>
    /// The folder to use when saving this to json
    /// </summary>
    public string TableName { get; }

    /// <summary>
    /// Extra sql columns for this type that aren't added by field attributes
    /// </summary>
    public Dictionary<string, Type> ExtraSqlColumns
      = new Dictionary<string, Type>();

    /// <summary>
    /// Option to set that this has no type field
    /// </summary>
    public DataModelAttribute(string ModelTypeName, Type ArchetypeBaseType = null, string[] ExtraSqlColumnNames = null, Type[] ExtraSqlColumnTypes = null) {
      TableName = ModelTypeName;
      HasTypeField = (this.ArchetypeBaseType = ArchetypeBaseType) != null;
      if (ExtraSqlColumnNames != null) {
        if (ExtraSqlColumnNames.Count() != ExtraSqlColumnTypes.Count()) {
          throw new IndexOutOfRangeException($"Must have = number of ExtraSqlColumnNames and ExtraSqlColumnTypes on model type: {ModelTypeName}.");
        }
        ExtraSqlColumns = ExtraSqlColumnNames.Zip(ExtraSqlColumnTypes, (key, value) => new { key, value }).ToDictionary(x => x.key, x => x.value);
      }
    }

    #region Tests

    /// <summary>
    /// Build a model for testing from a type:
    /// </summary>
    /// <returns></returns>
    internal IModel test_build_model(Type modelType) {
      IModel testModel;
      if (ArchetypeBaseType == null || typeof(Enumeration).IsAssignableFrom(ArchetypeBaseType)) {
        if (ArchetypeBaseType == null) {
          UnityEngine.Debug.LogWarning($"Cannot test model type: {modelType.FullName}. Skipping because there's no type to build a model with.");
          return null;
        } else {
          testModel = (IModel)Activator.CreateInstance(modelType, Enumeration.GetById(1, ArchetypeBaseType));
        }
      } // if it's an archetype we need to grab one that uses this model type.
      else {
        if (!ArchetypeCollection.CollectionsByBaseTypeClass.ContainsKey(ArchetypeBaseType.FullName)) {
          UnityEngine.Debug.LogWarning($"Cannot test model type: {modelType.FullName}. Skipping because no Archetypes are existant for this model type.");
          return null;
        }

        int? testTypeId = ArchetypeCollection.CollectionsByBaseTypeClass[ArchetypeBaseType.FullName].GetRandomTypeForModel(modelType)?.Id;
        if (testTypeId.HasValue) {
          testModel = ArchetypeCollection.CollectionsByBaseTypeClass[ArchetypeBaseType.FullName]
           .GetFromInternal(testTypeId.Value).MakeEmptyModel("testId");
        } else {
          UnityEngine.Debug.LogWarning($"Cannot test model type: {modelType.FullName}. Skipping because no Archetypes are existant for this model type.");
          return null;
        }
      }

      return testModel;
    }

    /// <summary>
    /// simple test for model serialization and deserialization:
    /// </summary>
    internal bool test_SerializationAndDeserialization(Type modelType) {
      if (!typeof(IModel).IsAssignableFrom((modelType))) {
        throw new NotImplementedException($"To use the DataModelAttribute, a type must inherit from IModel. {modelType.FullName} does not.");
      }

      // make a model of the given type:
      IModel testModel = test_build_model(modelType);
      if (testModel == null) {
        return false;
      }

      // serialize
      (System.Collections.Generic.Dictionary<string, object> sqlParams, Newtonsoft.Json.Linq.JObject JSONData) 
        = ModelSerializer.Serialize(testModel)[testModel.getTableName()][(testModel as IUniqueModel)?.id ?? ModelSerializer.SerializedModelDataCollection.NoIdDataKey].First();
      sqlParams.Merge(ModelSerializer.Cache.GetDataModelInfo(testModel).ExtraSqlColumns.ToDictionary(
        column => column.Key,
        column => column.Value.IsValueType ? Activator.CreateInstance(column.Value) : null
      ));

      // deserialize the data into a new model of the given type:
      IModel deserializedTestModel;
      if (ArchetypeBaseType == null || typeof(Enumeration).IsAssignableFrom(ArchetypeBaseType)) {
        deserializedTestModel = (IModel)Activator.CreateInstance(modelType, Enumeration.GetById(1, modelType));
        deserializedTestModel = ModelSerializer.Deserialize(deserializedTestModel, sqlParams, JSONData);
      } else {
        deserializedTestModel = ArchetypeCollection.CollectionsByBaseTypeClass[ArchetypeBaseType.FullName]
          .MakeAndDeserialize(sqlParams, JSONData);
      }

      // compare the new and old model:
      string original = null;
      string deserialized = null;
      try {
        original = JsonConvert.SerializeObject(testModel, Formatting.Indented);
        deserialized = JsonConvert.SerializeObject(deserializedTestModel, Formatting.Indented);
      } catch {}
      if (deserializedTestModel.GetType().Name == "Tile") {
        var test = "10000000";
        Console.WriteLine(test);
      }
      if (!deserializedTestModel.Equals(testModel) || original != deserialized) {
        throw new Exception($"Failed to assert deserialized model is equal to serialized model:\n\tOriginal:\n{original ?? testModel.ToString()}\n\tDeserialized:\n{deserialized ?? deserializedTestModel.ToString()}");
      }

      return true;
    }

    #endregion
  }
}
