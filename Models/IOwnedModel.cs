﻿namespace Meep.Tech.ModularData {
  /// <summary>
  /// A simple model that can't be light loaded
  /// Does NOT have a uniqueID, but the ID may at least describe it.
  /// Good for structs
  /// </summary>
  public interface IOwnedModel<ArchetypeType> : IOwnedModel where ArchetypeType : IMappable {

    /// <summary>
    /// Models of an archetype type should have a field named type exposing it
    /// </summary>
    ArchetypeType type {
      get;
    }
  }

  /// <summary>
  /// A simple model that is always owned by a parent model.
  /// Does NOT have a uniqueID.
  /// Good for structs.
  /// </summary>
  public interface IOwnedModel : IModel, IOwned {}
}
