﻿namespace Meep.Tech.ModularData {

  /// <summary>
  /// A model that can be saved to or loaded from the DB, fully or partially(aka light-loaded).
  /// This specifies the archetype field
  /// </summary>
  public interface IUniqueModel<ArchetypeType> : IUniqueModel where ArchetypeType : Archetype {

    /// <summary>
    /// Models of an archetype type should have a field named type exposing it
    /// </summary>
    ArchetypeType type {
      get;
    }
  }

  /// <summary>
  /// A unique model, can have child models.
  /// </summary>
  public interface IUniqueModel : IModel, IUnique {

    /// <summary>
    /// Save this model's data
    /// <param name="andAllChildrenRecursively">If this should save all child models and their children as well</param>
    /// </summary>
    virtual void save(bool andAllChildrenRecursively = true) {
      ModelSerializer.Save(
        this,
        andAllChildrenRecursively 
          ? ModelSerializer.SaveDepth.ModelData_AllRecusiveChildData
          : ModelSerializer.SaveDepth.ModelDataOnly
      );
    }

    /// <summary>
    /// load this model's data from external sources.
    /// This may only work for object based models, structs may see issues and need to override this
    /// <param name="andAllChildrenRecursively">If this should load all child models and their children as well</param>
    /// </summary>
    virtual void load(bool andAllChildrenRecursively = true) {
      ModelSerializer.Load(
        this,
        andAllChildrenRecursively 
          ? ModelSerializer.LoadDepth.Model_AllRecusiveChildren
          : ModelSerializer.LoadDepth.Model
      );
    }

    /// <summary>
    /// Save this model's data.
    /// </summary>
    virtual void save(ModelSerializer.SaveDepth saveDepth) {
      ModelSerializer.Save(
        this,
        saveDepth
      );
    }

    /// <summary>
    /// load this model from external sources.
    /// This may only work for object based models, structs may see issues and need to override this
    /// </summary>
    virtual void load(ModelSerializer.LoadDepth loadDepth) {
      ModelSerializer.Load(
        this,
        loadDepth
      );
    }
  }
}
