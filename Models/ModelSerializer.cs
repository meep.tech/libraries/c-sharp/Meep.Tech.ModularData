﻿using Mono.Data.Sqlite;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEngine;
using System.Text;
using System.Data;

namespace Meep.Tech.ModularData {

  public partial class ModelSerializer {
    
    /// <summary>
    /// The deph at which to load data for this model and it's hirerachal children
    /// Light means Load Light Data only, and light loading a child field means: don't get the full data from the DB and only load the model's ID and type etc.
    /// Full means Fully load all model data fields, and fully loading children means: fetch the docs for any children from the DB by ID.
    /// </summary>
    public enum LoadDepth {
      Model_AllRecusiveChildren = -1,
      Model = 0,
      Model_FirstChildren = 1,
      Model_FirstChildren_NextChildren = 2,
      Model_FirstChildren_NextChildren_ThenTheirChildren = 3
    }

    public enum SaveDepth {
      ModelData_AllRecusiveChildData = 0,
      ModelDataOnly = 1
    }

    #region Constants

    /// Column Names:
    public const string SQLIDColumnName = "id";
    public const string SQLTypeColumnName = "type";
    public const string SQLOwnerColumnName = "owner";
    public const string SQLOwnerModelFieldNameColumnName = "ownerModelFieldName";
    public const string SQLJSONDataColumnName = "data";

    /// <summary>
    /// Used to split columns in a list
    /// </summary>
    public const string SqlListDelimiter = ", \n\t";

    /// <summary>
    /// Used to map C# data types to SQL field table types
    /// </summary>
    public static Dictionary<Type, string> SqliteColumnDataTypeMappings = new Dictionary<Type, string> {
      {typeof(byte[]), "BLOB" },
      {typeof(int), "INTEGER" },
      {typeof(byte), "INTEGER" },
      {typeof(bool), "INTEGER" },
      {typeof(short), "INTEGER" },
      {typeof(string), "TEXT" },
      {typeof(DateTime), "TEXT" },
      {typeof(decimal), "TEXT" },
      {typeof(float), "REAL" },
      {typeof(double), "REAL" }
    };

    /// <summary>
    /// Used to map C# data types to SQL field table types
    /// </summary>
    public static Dictionary<Type, System.Data.DbType> SQLColumnDataTypeMappings = new Dictionary<Type, System.Data.DbType> {
      {typeof(byte[]), System.Data.DbType.Binary },
      {typeof(int), System.Data.DbType.Int64 },
      {typeof(byte), System.Data.DbType.Byte },
      {typeof(bool), System.Data.DbType.Boolean },
      {typeof(short), System.Data.DbType.Int16 },
      {typeof(string), System.Data.DbType.String },
      {typeof(DateTime), System.Data.DbType.DateTime },
      {typeof(decimal), System.Data.DbType.Decimal },
      {typeof(float), System.Data.DbType.Decimal },
      {typeof(double), System.Data.DbType.Decimal }
    };

    /// <summary>
    /// The main DB name
    /// </summary>
    public static string DBName = "Models.db";

    #endregion

    /// <summary>
    /// Connection to the DB for model JSON storage.
    /// </summary>
    public static SqliteConnection ModelDBConnection {
      get;
      private set;
    }

    #region Initialization

    /// <summary>
    /// Initialize the DB location
    /// </summary>
    /// <param name="rootFileSystemSavePath"></param>
    public static void Initialize(string rootFileSystemSavePath) {
      ModelDBConnection = new SqliteConnection(
        $"Data Source={Path.Combine(rootFileSystemSavePath, "DB", DBName)}"
      );
      ModelDBConnection.Open();
    }

    #endregion

    #region Save & Load

    /// <summary>
    /// Save this model to the DB by it's unique id
    /// </summary>
    internal static void Save<ModelType>(ModelType model, SaveDepth saveDepth = SaveDepth.ModelData_AllRecusiveChildData) where ModelType : IUniqueModel {
      // get the fields data we need from the cache
      DataModelAttribute modelDataInfo = Cache.GetDataModelInfo(model);
      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> modelDataFields
        = Cache.GetFieldsInfo(model);

      Dictionary<string, Dictionary<string, IEnumerable<SerializedData>>> serializedModelDatas = Serialize(model);
        //.SelectMany(tables => tables.Value)
        //.SelectMany(id => id.Value);

      SaveModelDatas(serializedModelDatas);
    }

    /// <summary>
    /// Load data from storage into this model based on it's ID
    /// </summary>
    internal static ModelType Load<ModelType>(ModelType model, LoadDepth loadDepth = LoadDepth.Model_AllRecusiveChildren) where ModelType : IUniqueModel {
      // get the fields data we need from the cache
      DataModelAttribute modelDataInfo = Cache.GetDataModelInfo(model);
      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> modelDataFields
        = Cache.GetFieldsInfo(model);

      /// Get the root model's data
      SerializedData serializedModelData = GetModelData(
        model.id,
        modelDataInfo.TableName,
        modelDataFields.Where(field => field.dataModelFieldInfo.IsASQLField)
      );

      /// deserialize the base model data
      model = Deserialize(model, serializedModelData.sqlData, serializedModelData.jsonData, modelDataFields);

      /// load all the children to the specified depth
      if (loadDepth != 0) {
        model = LoadAllChildModels(
          model, 
          loadDepth == LoadDepth.Model_AllRecusiveChildren 
            ? (int?)null 
            : (int)loadDepth - 1
        );
      }

      return model;
    }

    /// <summary>
    /// Load all child models for the given model:
    /// </summary>
    internal static ModelType LoadAllChildModels<ModelType>(ModelType model, int? depthLimit = null) where ModelType : IUniqueModel {
      // cache data for parent:
      Type parentModelType = model.GetType();
      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> parentModelDataFieldsInfo
        = Cache.GetFieldsInfo(parentModelType);
      if (!typeof(IUniqueModel).IsAssignableFrom(parentModelType)) {
        throw new NotImplementedException($"Cannot grab child models for a model type that is not unique: {parentModelType.FullName}");
      }

      // for each child model field, load all the child models:
      foreach ((MemberInfo objectFieldInfo, ChildModelFieldAttribute dataModelFieldInfo) childModelField in parentModelDataFieldsInfo.Where(field => field.dataModelFieldInfo is ChildModelFieldAttribute)) {
        object deserializedChildModelDataToSet;

        // image fields are loaded as children
        if (childModelField.dataModelFieldInfo is ModelImageFieldAttribute imageFieldInfo) {
          Dictionary<string, object> sqlRowData 
            = GetDataForModelImage(model, childModelField.dataModelFieldInfo.Name ?? childModelField.objectFieldInfo.Name);

          // set the image as the value to set if it exists:
          if (sqlRowData?[ModelImageFieldAttribute.ImageBytesColumnName] is byte[] imageBytes) {
            deserializedChildModelDataToSet = imageFieldInfo.DeserializeFromBytes(imageBytes);
          } else continue;
        } // Linked model fields (not children, but a whole model linked to this one that we  load like a child model)
          
        // Normal child model field:
        else {
          List<IModel> childModelsForThisFieldType = new List<IModel>();
          foreach (SerializedData serializedChildModelData in GetDataForChildModels(model.id, childModelField.dataModelFieldInfo.ModelType, childModelField.dataModelFieldInfo.Name ?? childModelField.objectFieldInfo.Name)) {
            DataModelAttribute childFieldModelData = Cache.GetDataModelInfo(childModelField.dataModelFieldInfo.ModelType);
            // if this is an owend model, record the name of the field it's from as well
            if (serializedChildModelData.sqlData.ContainsKey(SQLOwnerColumnName)) {
              serializedChildModelData.sqlData[SQLOwnerModelFieldNameColumnName] = childModelField.dataModelFieldInfo.Name ?? childModelField.objectFieldInfo.Name;
            }
            serializedChildModelData.sqlData.TryGetValue(SQLTypeColumnName, out object typeData);
            string childModelArchetypeId = typeData as string;
            // if it's unique, we need to add it to the uniqle list and recusively get it's children
            if (childModelField.dataModelFieldInfo.IsUnique) {
              string childModelUniqueId = serializedChildModelData.sqlData[SQLIDColumnName] as string;
              IModel deserializedModel = childModelField.dataModelFieldInfo.HasAdvancedChildDeserialization
                ? childModelField.dataModelFieldInfo.ChildModelAdvancedDeserializationFunction(
                  childModelUniqueId,
                  childModelArchetypeId,
                  serializedChildModelData,
                  model
                )
                : childModelField.dataModelFieldInfo.ChildModelDeserializationFunction(
                  childModelUniqueId,
                  childModelArchetypeId,
                  serializedChildModelData
                );
              childModelsForThisFieldType.Add(deserializedModel);

              /// if we still have depth to descend, get the recusive children of the child model and add them too:
              if (!depthLimit.HasValue || depthLimit.Value > 0) {
                LoadAllChildModels(deserializedModel as IUniqueModel, depthLimit - 1);
              }
            } // if it's not unique it's just a simple deserialize:
            else {
              childModelsForThisFieldType.Add(childModelField.dataModelFieldInfo.HasAdvancedChildDeserialization
                ? childModelField.dataModelFieldInfo.ChildModelAdvancedDeserializationFunction(
                  "",
                  childModelArchetypeId,
                  serializedChildModelData,
                  model
                )
                : childModelField.dataModelFieldInfo.ChildModelDeserializationFunction(
                  "",
                  childModelArchetypeId,
                  serializedChildModelData
                )
              );
            }
          }

          // if this is a multi model child field then set set the whole list, else just grab the first and only item from it:
          deserializedChildModelDataToSet = childModelField.dataModelFieldInfo is ChildModelsCollectionFieldAttribute
              ? (object)childModelsForThisFieldType
              : childModelsForThisFieldType.First();
        }

        // set the deserialized values to their field (if we want to):
        if (!childModelField.dataModelFieldInfo.SkipSettingValueToFieldAfterDeserialzation) {
          // set the child model to it's field:
          if (childModelField.objectFieldInfo is FieldInfo field) {
            field.SetValue(model, deserializedChildModelDataToSet);
          } else if (childModelField.objectFieldInfo is PropertyInfo property) {
            property.SetMethod.Invoke(model, deserializedChildModelDataToSet?.AsArray());
          }
        }
      }

      return model;
    }

    #endregion

    #region Serialization and Deserialization

    /// <summary>
    /// Serialize a model's data
    /// </summary>
    internal static SerializedModelDataCollection Serialize<ModelType>(
      ModelType model,
      bool thisModelsDataOnly = false
    ) where ModelType : IModel {
      SerializedModelDataCollection serializedModels
        = new SerializedModelDataCollection();

      if (model == null) {
        return serializedModels;
      }

      string parentModelId = null;

      // get the fields data we need from the cache
      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> modelDataFields
        = Cache.GetFieldsInfo(model);
      string parentModelTableName = Cache.GetDataModelInfo(model).TableName;

      // create the root json object
      JObject modelJSONFields = new JObject();
      Dictionary<string, object> modelSQLFields = new Dictionary<string, object>();
      foreach ((MemberInfo field, ModelDataFieldAttribute serializationInfo) in modelDataFields) {
        // skip these, they must be serialized manually
        if (field == null) {
          continue;
        }

        /// Try to get the field data:
        try {
          object fieldValue = field.GetValue(model);

          // If this is a child field, ignore it if we only want this model's data, unless it's not unique to it's own table.
          if (serializationInfo is ChildModelFieldAttribute childFieldInfo) {

            // models without unique ids are always saved with their parent
            if (fieldValue != null && (!thisModelsDataOnly || !childFieldInfo.IsUnique)) {
              /// add the special delete info data:
              if (childFieldInfo is ChildModelsCollectionFieldAttribute childCollection && childCollection.DeleteAllOldChildrenBeforeSavingNewChildrenOfThisType) {
                parentModelId = (model as IUnique).id;
                serializedModels.addSerializedModelToCollectionByTableNameThenId(
                  SerializedModelDataCollection.DeleteDataKey,
                  SerializedData.MakeDeletionEntry(
                    parentModelId,
                    childCollection.TableName,
                    childCollection.Name ?? field.Name
                  )
                );
              }

              // serialize the child field models
              SerializedModelDataCollection childModels = (childFieldInfo.HasAdvancedChildSerialization
                ? childFieldInfo.ChildModelAdvancedSerializationFunction(fieldValue, model)
                : childFieldInfo.ChildModelSerializationFunction(fieldValue)
              );

              // TODO: this is probably a bit costly, try to find a better way to attach the name or an id from the field it's from to the direct child models only:
              // if there are any, set their owner field and merge them
              if (childModels.Any() && childModels.ContainsKey(childFieldInfo.TableName)) {
                // for each model collection of this type
                childModels[childFieldInfo.TableName].ForEach(
                  // (all models at this point should have an owner column)
                  @if: (modelDatas) => /*modelDatas.Value.Any(modelData => modelData.sqlData?.ContainsKey(SQLOwnerColumnName) ?? false)
                    // if there's an item with no parent field name column yet, or an empty one:
                    &&*/ modelDatas.Value.Any(modelData => !(modelData.sqlData?.ContainsKey(SQLOwnerModelFieldNameColumnName) ?? false))
                      || (modelDatas.Value.Any(modelData => (modelData.sqlData?.ContainsKey(SQLOwnerModelFieldNameColumnName) ?? false)
                        && string.IsNullOrEmpty((string)modelData.sqlData[SQLOwnerModelFieldNameColumnName])
                      )),
                  @do: (modelDatas) => {
                    /// foreach of the individual model datas
                    modelDatas.Value.ForEach(
                        // if it has an owner column that matches this parent model id:
                      @if: childModel => childModel.sqlData.ContainsKey(SQLOwnerColumnName) 
                        && ((string)childModel.sqlData?[SQLOwnerColumnName] ?? null) == (parentModelId ??= (model as IUnique).id),
                      // set the owner field:
                      @do: childModel => childModel.setParentModelFieldIfEmpty(childFieldInfo.Name ?? field.Name)
                    );
                  }
                );

                /// Get the fields base value:
                serializedModels.Merge(childModels);
              }
            }

            continue;
          } // image field saving
          else if (serializationInfo is ModelImageFieldAttribute imageFieldInfo) {
            if (model is IUnique parentModel) {
              parentModelId = parentModel.id;
              // if there's a dirty check generated, check if it's dirty, else always save it
              if (imageFieldInfo.CheckisDirty?.Invoke(model) ?? true) {
                serializedModels.addSerializedModelToCollectionByTableNameThenId((
                  ModelImageFieldAttribute.ImageTableName,
                  imageFieldInfo.SerializeToSQLRow(
                    parentModel.id,
                    serializationInfo.Name ?? field.Name,
                    parentModelTableName,
                    fieldValue
                  ),
                  null
                ));
              }

              continue;
            } else throw new NotImplementedException($"Cannot save images on non-IUnique models. Images are saved by parent model id");
          } // apply any custom serialization to other fields:
          else if (serializationInfo.HasCustomSerialization) {
            fieldValue = serializationInfo.SerializationFunction(fieldValue);
          }

          // if this a sql field, we need to add it to the sql list with it's field name:
          if (serializationInfo.IsASQLField) {
            string column = serializationInfo.Name ?? field.Name;
            modelSQLFields.Add(column, fieldValue);
          } // If this is a json field, add it to the json:
          else {
            JToken serializedFieldData = fieldValue is JToken serializedFieldValue
              ? serializedFieldValue
              : fieldValue == null
                ? JValue.CreateNull()
                : JToken.Parse(JsonConvert.SerializeObject(fieldValue));

            // add the field to the jsons
            string key = serializationInfo.Name ?? field.Name;
            modelJSONFields.Add(key, serializedFieldData);
          }
        } catch (Exception e) {
          throw new Exception($"Failed to serialize the field: {field.Name}, on the model: {model}, of type: {model.GetType()}.\nINTERNAL ERROR: {e}");
        }
      }

      // place this model's data first
      serializedModels.prependSerializedModelToCollectionByTableNameThenId(
        parentModelId 
          ?? (model is IUnique uniqueModel 
            ? uniqueModel.id 
            : null) 
          ?? SerializedModelDataCollection.NoIdDataKey,
        (Cache.GetDataModelInfo(model).TableName, modelSQLFields, modelJSONFields)
      );

      return serializedModels;
    }

    /// <summary>
    /// Deserialize a model from data
    /// </summary>
    internal static ModelType Deserialize<ModelType>(
      ModelType model,
      Dictionary<string, object> serializedModelSQLFields,
      JObject serializedModelJSONFields,
      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> modelDataFields = null
    ) where ModelType : IModel {
      // get the fields data we need from the cache
      modelDataFields ??= Cache.GetFieldsInfo(model);

      /// deserialize each field onto the model:
      foreach ((MemberInfo fieldInfo, ModelDataFieldAttribute fieldSerializationInfo) in modelDataFields) {
        // if we don't deserialize this field
        if (fieldSerializationInfo.SkipDuringDeserialization) {
          continue;
        }

        // if this is a child model field, we want to skip it. They get loaded through LoadAllChildModels
        if (fieldSerializationInfo.IsChildModelField) {
          continue;
        }

        try {
          // get the serialized data from the right place:
          object serializedFieldData = fieldSerializationInfo.IsASQLField
            ? serializedModelSQLFields[fieldSerializationInfo.Name ?? fieldInfo.Name]
            : serializedModelJSONFields.GetValue(fieldSerializationInfo.Name ?? fieldInfo.Name);

          /// advanced deserialization is special
          if (fieldSerializationInfo.HasAdvancedJSONFieldDeserialization) {
            model = (ModelType)fieldSerializationInfo.AdvancedJSONDeserializationFunction((JToken)serializedFieldData, model);
          } else {
            // if there's simple custom deserialization:
            object dataToSet = fieldSerializationInfo.HasCustomDeserialization
              // use the specified method of deserialization:
              ? fieldSerializationInfo.DeserializeObject?.Invoke(serializedFieldData) ?? fieldSerializationInfo.DeserializeJSON(serializedFieldData as JToken)
              // if it's just standard json deserialize it, else just take what it gave us
              : serializedFieldData is JToken jsonFieldData
                ? jsonFieldData.ToObject(fieldInfo.DataType())
                : serializedFieldData;

            // set the deserialized data to the field or property
            try {
              if (!fieldInfo.DataType().IsAssignableFrom(dataToSet?.GetType() ?? null) && dataToSet != null) {
                dataToSet = dataToSet.CastTo(fieldInfo.DataType());
              }
              if (fieldInfo is FieldInfo field) {
                field.SetValue(model, dataToSet);
              } else if (fieldInfo is PropertyInfo property) {
                if (property.SetMethod != null) {
                  property.SetMethod.Invoke(model, dataToSet.AsArray());
                } else {
                  if (!property.CanWrite) {
                    throw new NotImplementedException($"Property: {property.Name}, on model type: {model.GetType()}, cannot be deserialized to as it is not a writeable property.");
                  }
                  property.GetSetMethod(true)?.Invoke(model, dataToSet?.AsArray());
                }
              }
            } catch (NotImplementedException ne) {
              throw ne;
            } catch (Exception e) {
              throw new MethodAccessException($"Property of Field: {fieldInfo.Name}, on model type: {model.GetType()}, cannot be deserialized to due to errors during the SET attempt:\n{e}");
            }
          }
        } catch (Exception e) {
          throw new Exception($"Failed to deserialize the field: {fieldInfo.Name}, on the model: {model}, of type: {model.GetType()}.\nINTERNAL ERROR: {e}");
        }
      }

      model.finishDeserialization();
      return model;
    }

    #endregion

    #region DB Access

    /// <summary>
    /// Get the model based on ID without child data
    /// </summary>
    public static SerializedData GetModelData(string modelUniqueId, Type modelType) {
      string modelTableName = Cache.GetDataModelInfo(modelType).TableName;
      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> childModelFieldsInfo
        = Cache.GetFieldsInfo(modelType);
      IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> sqlFieldsForThisModelType
        = childModelFieldsInfo.Where(field => field.dataModelFieldInfo.IsASQLField);

      return GetModelData(modelUniqueId, modelTableName, sqlFieldsForThisModelType);
    }

    /// <summary>
    /// Get the model based on ID without child data
    /// </summary>
    public static SerializedData GetModelData<ModelType>(string modelUniqueId) where ModelType : IUnique {
      return GetModelData(modelUniqueId, typeof(ModelType));
    }

    /// <summary>
    /// Get the model based on ID without child data
    /// </summary>
    static SerializedData GetModelData(
      string modelUniqueId,
      string modelTableName,
      IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> sqlFieldsForThisModelType
    ) {
      SqliteCommand query = ModelDBConnection.CreateCommand();
      (IEnumerable<string> sqlColumns, string tableAssuranceQuery, _) = Cache.GetTableAssuranceQuery(modelTableName, sqlFieldsForThisModelType);
      query.CommandText = tableAssuranceQuery
        + "SELECT " +
            sqlColumns.Join(SqlListDelimiter) + @"
          FROM " + modelTableName + " " +
         "WHERE id = ?";
      query.Parameters.Add(modelUniqueId);
      SqliteDataReader modelDataRow = query.ExecuteReader();
      Dictionary<string, object> sqlDataRows = new Dictionary<string, object>();
      JObject modelJSON = new JObject();
      if (modelDataRow.Read()) {
        int columnOrdinalIndex = 0;
        // go though each column we put in and grab the result:
        foreach ((MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo) field in sqlFieldsForThisModelType) {
          sqlDataRows.Add(field.dataModelFieldInfo.Name ?? field.objectFieldInfo.Name, modelDataRow.GetValue(columnOrdinalIndex++));
        }

        /// Json should be the last column fetched, named 'data'.
        modelJSON = JObject.Parse(modelDataRow.GetString(columnOrdinalIndex));
      }

      return (modelTableName, sqlDataRows, modelJSON);
    }

    /// <summary>
    /// Get all the model data for a single child model type:
    /// </summary>
    public static List<SerializedData> GetDataForChildModels(
      string parentModelUniqueId,
      Type childModelType,
      string parentModelFieldName
    ) {
      // Get type data from cache:
      DataModelAttribute childModelInfo = Cache.GetDataModelInfo(childModelType);
      string modelTableName = childModelInfo.TableName;
      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> childModelFieldsInfo
        = Cache.GetFieldsInfo(childModelType);
      IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> sqlFieldsForThisModelType
        = childModelFieldsInfo.Where(field => field.dataModelFieldInfo.IsASQLField);

      SqliteCommand query = ModelDBConnection.CreateCommand();
      (IEnumerable<string> sqlColumnsToQuery, string tableAssuranceQuery, _) = Cache.GetTableAssuranceQuery(modelTableName, sqlFieldsForThisModelType);
      query.CommandText = tableAssuranceQuery
        + "SELECT " +
            sqlColumnsToQuery.Join(SqlListDelimiter) + @"
           FROM " + modelTableName + " " +
         "WHERE " + SQLOwnerColumnName + @" = ?
           AND " + SQLOwnerModelFieldNameColumnName + " = ?";
      query.Parameters.Add(parentModelUniqueId);
      query.Parameters.Add(parentModelFieldName);
      SqliteDataReader resultingRows = query.ExecuteReader();

      // reach each row into a list item
      List<SerializedData> childModelDatas = new List<SerializedData>();
      while (resultingRows.Read()) {
        int columnOrdinalIndex = 0;
        // go though each column we put in and grab the result:
        Dictionary<string, object> sqlDataRows = new Dictionary<string, object>();
        foreach ((MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo) field in sqlFieldsForThisModelType) {
          sqlDataRows.Add(field.dataModelFieldInfo.Name ?? field.objectFieldInfo.Name, resultingRows.GetValue(columnOrdinalIndex++));
        }

        /// Json should be the last column fetched, named 'data':
        childModelDatas.Add((
          modelTableName,
          sqlDataRows,
          JObject.Parse(resultingRows.GetString(columnOrdinalIndex))
        ));
      }

      return childModelDatas;
    }

    /// <summary>
    /// Get all the model data for a single child model type:
    /// </summary>
    public static IEnumerable<SerializedData> GetDataForChildModels<ChildModelType>(
      string parentModelUniqueId,
      string childFieldName
    ) {
      return GetDataForChildModels(parentModelUniqueId, typeof(ChildModelType), childFieldName);
    }

    /// <summary>
    /// Get the image for a model given the field the image is from
    /// </summary>
    static Dictionary<string, object> GetDataForModelImage<ModelType>(ModelType model, string imageFieldName) where ModelType : IUniqueModel {
      Dictionary<string, object> sqlRowData = new Dictionary<string, object>();
      string modelTypeName = Cache.GetDataModelInfo(model).TableName;

      // Make the sql command to fetch the image data
      SqliteCommand query = ModelDBConnection.CreateCommand();
      (IEnumerable<string> sqlColumnsToQuery, string tableAssuranceQuery, _) = Cache.GetTableAssuranceQuery(ModelImageFieldAttribute.ImageTableName, null);
      query.CommandText = tableAssuranceQuery
        + "SELECT " +
            sqlColumnsToQuery + @"
           FROM " + ModelImageFieldAttribute.ImageTableName + " " +
         $"WHERE {ModelImageFieldAttribute.ImageOwnerColumnName} = ? " +
           $"AND {ModelImageFieldAttribute.ImageOwnerTypeColumnName} = ? " +
           $"AND {ModelImageFieldAttribute.ImageOwnerFieldColumnName} = ?;";
      query.Parameters.AddRange(new string[] { model.id, modelTypeName, imageFieldName });

      // execute and read the query results:
      SqliteDataReader resultingRow = query.ExecuteReader();
      if (resultingRow.Read()) {
        int columnIndex = 0;
        foreach (string columnName in sqlColumnsToQuery) {
          sqlRowData.Add(columnName, resultingRow.GetValue(columnIndex++));
        }
      }

      return sqlRowData;
    }


    /// <summary>
    /// Save serialized model data to the db
    /// </summary>
    static void SaveModelDatas(Dictionary<string, Dictionary<string, IEnumerable<SerializedData>>> serializedModelDatas) {
      // Lets build a BIG query lol
      SqliteCommand query = ModelDBConnection.CreateCommand();
      StringBuilder queryString = new StringBuilder();

      // batch all the models that are in the same tables:
      string currentTable = null;
      IEnumerable<string> currentTableSqlColumnNames = default;
      Dictionary<string, Type> expectedColumnTypes = default;

      /// for each table we need to add model data rows for:
      foreach((string tableName, Dictionary<string, IEnumerable<SerializedData>> serializedModelDatasById) in serializedModelDatas) {

        // if there's a query for another table already in progress we need to finish it:
        if (currentTable != null) {
          queryString.Append(";\n");
        }
        currentTable = tableName;

        // make sure to initialize this table:
        string tableAssuranceQuery;
        (currentTableSqlColumnNames, tableAssuranceQuery, expectedColumnTypes)
          = Cache.GetTableAssuranceQuery(currentTable, Cache.GetSqlFieldsForTable(tableName));
        queryString.Append($"\n{tableAssuranceQuery}");

        // check if we have deletes for this table based on a parent id. If we do, do those first for the table:
        if (serializedModelDatasById.TryGetValue(SerializedModelDataCollection.DeleteDataKey, out IEnumerable<SerializedData> tableDeletionDatas)) {
          foreach(SerializedData deletionData in tableDeletionDatas) {
            queryString.Append($"DELETE FROM {deletionData.tableName}\n");
            queryString.Append($"   WHERE {SQLOwnerColumnName} = ?\n");
            queryString.Append($"      AND {SQLOwnerModelFieldNameColumnName} = ?;\n\n");

            query.Parameters.Add(new SqliteParameter(System.Data.DbType.String, deletionData.sqlData[SQLOwnerColumnName]) { SourceColumn = SQLOwnerColumnName });
            query.Parameters.Add(new SqliteParameter(System.Data.DbType.String, deletionData.sqlData[SQLOwnerModelFieldNameColumnName]) { SourceColumn = SQLOwnerModelFieldNameColumnName });

          }

          // Remove the deletes:
          serializedModelDatasById.Remove(SerializedModelDataCollection.DeleteDataKey);

          // if this was just a delete, continue without adding values
          if (!serializedModelDatasById.Any()) {
            continue;
          }
        }

        /// Add the rows as VALUEs
        // Initialize the upsert:
        queryString.Append($"INSERT INTO {currentTable} (\n\t");
        queryString.Append(currentTableSqlColumnNames.Join(SqlListDelimiter));
        queryString.Append($"\n) VALUES");

        // foreach set of model data under this id key add a row to the DB:
        int currentModelCount = 0;
        int totalModels = serializedModelDatasById.SelectMany(values => values.Value).Count();
        foreach ((string modelId, IEnumerable<SerializedData> serializedDatas) in serializedModelDatasById) {
          foreach (SerializedData serializedData in serializedDatas) {
            if (currentModelCount++ != 0) {
              queryString.Append(",");
            }
            queryString.Append($" (\n\t");
            int currentRowAddedCount = 0;

            /// For each row of data to add in VALUES:
            // get all the columns and values we're using.
            currentTableSqlColumnNames
              // use null and rely on the DB to provide a default value:
              .Select(columnName => (name: columnName, value: serializedData.sqlData.TryGetValue(columnName, out object columnValue)
                ? columnValue
                : null))
              // add json data to the end:
              .SkipLast(1)
              .Append((SQLJSONDataColumnName, serializedData.jsonData?.ToString()))
              .ForEach(column => {
                /// For each column of data to add in VALUES:
                // add the VALUES ?s and +1 ? for json data too
                queryString.Append("?");
                // delimit if this isn't the last item in the list:
                if (++currentRowAddedCount != currentTableSqlColumnNames.Count()) {
                  queryString.Append(SqlListDelimiter);
                } // if it is the last item:
                else {
                  // if this is a model by unique id we want to upsert, not insert:
                  if (currentModelCount == totalModels && currentTableSqlColumnNames.First() == SQLIDColumnName) {
                    // TODO: add a toggle that allows: on conflict, use JSON_MERGE_PATCH() to patch json changes onto the new one, to keep removed mod data.
                    queryString.Append($"\n) ON CONFLICT ({SQLIDColumnName}) DO UPDATE SET\n");
                    int currentColumn = 1;
                    currentTableSqlColumnNames.Skip(1).ForEach(column => {
                      queryString.Append("\t");
                      queryString.Append(column);
                      queryString.Append(" = excluded.");
                      queryString.Append(column);
                      queryString.Append(++currentColumn != currentTableSqlColumnNames.Count() ? ",\n" : "");
                    });
                  } // the on conflict doesn't need a closing bracket, Values does
                  else {
                    queryString.Append($"\n)");
                  }
                }

                /// try to set up the sqlite param:
                try {
                  SqliteParameter param;
                  try {
                    param = new SqliteParameter(SQLColumnDataTypeMappings[expectedColumnTypes[column.name]], column.value ?? DBNull.Value) {
                      SourceColumn = column.name
                    };
                  } catch (Exception e) {
                    throw new InvalidCastException($"Could not make a new SqliteParameter for value object of type: {column.value?.GetType().FullName ?? "null"}, with value: {column.value?.ToString() ?? "null"}. Tried to cast to DBtype: {(SQLColumnDataTypeMappings.TryGetValue(column.value?.GetType(), out var dbType) ? dbType.ToString() : "NO DBTYPE FOR THIS TYPE")}\n{e}");
                  }

                  query.Parameters.Add(param);
                } catch (InvalidCastException e) {
                  throw new InvalidCastException($"Could not cast object: {column.value?.ToString() ?? "null"}, of type: {column.value?.GetType().FullName ?? "null"}, to a type SQL can use.\n\nQuery:{query.CommandText}\n\nCurrentParamIndex:{query.Parameters.Count}\n\n{e}");
                }
              });
          }
        }
      }

      // Finalize and build the command string:
      queryString.Append(";");
      query.CommandText = queryString.ToString();

      string queryText = query.CommandText;

      /*foreach (SqliteParameter p in query.Parameters) {
        queryText = ReplaceFirst(
          queryText,
          string.IsNullOrEmpty(p.ParameterName) ? "?" : p.ParameterName,
          p.DbType == DbType.Int64 ? p.Value.ToString() : $"'{p.Value.ToString()}'"
        );
      }

      File.WriteAllText(Application.persistentDataPath + "/test", queryText);//query.CommandText + "\n\nvalues:\n\n" + query.Parameters.Cast<SqliteParameter>().Select(param => $"{(string.IsNullOrEmpty(param.SourceColumn) ? "?" : $"?{param.SourceColumn}")}::{param.Value?.ToString() ?? "null"}").Join("\n"));
      */Debug.Log("Successfuly Built Query. Running...");
      Debug.Log($"Query Finished! {query.ExecuteNonQuery()} rows effected.");
    }

    /// <summary>
    /// testing, remove
    /// </summary>
    /// <param name="text"></param>
    /// <param name="search"></param>
    /// <param name="replace"></param>
    /// <returns></returns>
    public static string ReplaceFirst(string text, string search, string replace) {
      int pos = text.IndexOf(search);
      if (pos < 0) {
        return text;
      }
      return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
    }

    #endregion

    #region Cache

    /// <summary>
    /// Cache for storing data
    /// </summary>
    internal static class Cache {

      /// <summary>
      /// Cached model info
      /// </summary>
      static readonly Dictionary<string, DataModelAttribute> CachedDataModelInfo
        = new Dictionary<string, DataModelAttribute>();

      /// <summary>
      /// Cached model field info
      /// </summary>
      static readonly Dictionary<string, List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)>> CachedDataModelFieldsInfo
        = new Dictionary<string, List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)>>();

      /// <summary>
      /// Cached model field info
      /// </summary>
      static readonly Dictionary<string, List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)>> CachedSqlFieldsForTablesInfo
        = new Dictionary<string, List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)>>();

      /// <summary>
      /// Cached data queries
      /// </summary>
      static readonly Dictionary<string, (IEnumerable<string>, string)> CachedTableQueries
        = new Dictionary<string, (IEnumerable<string>, string)>();

      /*static readonly DictionaryExtensions.TrippleKeyDictionary<string, string, string, string> CachedParentModelFieldNames
        = new DictionaryExtensions.TrippleKeyDictionary<string, string, string, string>();*/

      /// <summary>
      /// Get the data model info
      /// </summary>
      /// <param name="dataModel"></param>
      /// <returns></returns>
      internal static DataModelAttribute GetDataModelInfo(object dataModel)
        => GetDataModelInfo(dataModel.GetType());

      /// <summary>
      /// Get the sql table rows for a given table:
      /// </summary>
      internal static IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> GetSqlFieldsForTable(string tableName) {
        if (CachedSqlFieldsForTablesInfo.TryGetValue(tableName, out var sqlColumnsInfo)) {
          return sqlColumnsInfo;
        } else throw new Exception($"SQL column data was never cached!");
      }

      /// <summary>
      /// Get the data model info
      /// </summary>
      /// <param name="dataModel"></param>
      /// <returns></returns>
      internal static DataModelAttribute GetDataModelInfo(Type dataModelType)
        => CachedDataModelInfo.TryGetValue(dataModelType.FullName, out DataModelAttribute dataModelAttribute)
          ? dataModelAttribute
          : (CachedDataModelInfo[dataModelType.FullName] = (DataModelAttribute)Attribute.GetCustomAttribute(dataModelType, typeof(DataModelAttribute)));

      /// <summary>
      /// Get the data for the fields that we should serialize
      /// </summary>
      internal static List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> GetFieldsInfo(Type modelType) {
        List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> modelDataFields
          = CachedDataModelFieldsInfo.TryGetValue(modelType.FullName, out var cachedDataFieldsInfo)
            ? cachedDataFieldsInfo
            : null;

        // if we have no fields at all, or we need the children fields and we're missing them:
        if (modelDataFields == null) {
          BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
          IEnumerable<MemberInfo> fields = modelType.GetFields(bindingFlags).Cast<MemberInfo>()
            .Concat(modelType.GetProperties(bindingFlags));

          // make new list(s)
          modelDataFields
            = new List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)>();

          /// if we have no data, fetch it:
          // validation:
          int idFieldCount = 0;
          int typeFieldCount = 0;
          foreach (MemberInfo field in fields) {
            if (field.GetCustomAttribute<ModelDataFieldAttribute>(true) is ModelDataFieldAttribute dataModelFieldInfo
              && field.GetCustomAttribute<IgnoreInheritedModelDataField>(true) == null
            ) {
              if (dataModelFieldInfo is ChildModelFieldAttribute && !typeof(IUniqueModel).IsAssignableFrom(modelType)) {
                throw new NotImplementedException($"Child models cannot be on non-IUnique parents");
              }
              // validation:
              if (dataModelFieldInfo is ModelIdFieldAttribute) {
                idFieldCount++;
              }
              if (dataModelFieldInfo is ModelTypeFieldAttribute) {
                typeFieldCount++;
              }
              modelDataFields.Add((field, dataModelFieldInfo));
            }
          }

          // add all extra sql fields, so they get validated for table creations:
          foreach ((string columnName, Type columnType) in GetDataModelInfo(modelType).ExtraSqlColumns) {
            // validation:
            if (columnName == SQLIDColumnName) {
              idFieldCount++;
            }
            if (columnName == SQLTypeColumnName) {
              typeFieldCount++;
            }
            modelDataFields.Add((
              null,
              new ModelDataFieldAttribute(
                columnName,
                CustomSerializeToType: columnType,
                IsASQLField: true,
                SkipDuringDeserialization: true
              )
            ));
          }

          // if this has an owner field, then add the owner row name field field to it's sql data
          if (modelDataFields.Any(item => item.dataModelFieldInfo.Name == SQLOwnerColumnName)) {
            modelDataFields.Add((
              null,
              new ModelDataFieldAttribute(
                SQLOwnerModelFieldNameColumnName,
                CustomSerializeToType: typeof(string),
                IsASQLField: true,
                SkipDuringDeserialization: true
              )
            ));
          }

          ///validation:
          // there must be exactly one type field per model
          if (idFieldCount > 1) {
            throw new MissingFieldException($"Model of type {modelType} cannot be serialized. In order to be used with the ModelJSONSerializer, a model's class must impliment the ModelIdFieldAttribute on a single field or property within in itself or it's inheritance tree.");
          }
          // there must be exactly one type field per model
          if (typeFieldCount > 1) {
            throw new MissingFieldException($"Model of type {modelType}, cannot be serialized. In order to be used with the ModelJSONSerializer, a model's class must impliment the ModelTypeFieldAttribute on a single field or property within in itself or it's inheritance tree.");
          }
          // there must be exactly one type field per model
          if (typeFieldCount > 1) {
            throw new MissingFieldException($"Model of type {modelType}, cannot be serialized. In order to be used with the ModelJSONSerializer, a model's class must impliment the ModelTypeFieldAttribute on a single field or property within in itself or it's inheritance tree.");
          }

          CachedSqlFieldsForTablesInfo[GetDataModelInfo(modelType).TableName] = modelDataFields.Where(field => field.dataModelFieldInfo.IsASQLField).ToList();
        }

        // update the caches and return
        return (CachedDataModelFieldsInfo[modelType.FullName] = modelDataFields);
      }

      /// <summary>
      /// Shortcut
      /// </summary>
      internal static List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> GetFieldsInfo(object dataModel)
        => GetFieldsInfo(dataModel.GetType());

      /// <summary>
      /// Get the columns to fetch for a table and a query to set up the table just in case
      /// </summary>
      internal static (
        IEnumerable<string> sqlColumnNames,
        string tableCreationAssuranceQuery,
        Dictionary<string, Type> columnDataTypes
      ) GetTableAssuranceQuery(
        string tableName,
        IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> sqlFieldsForThisModelType
      ) {
        IEnumerable<string> columnsToFetch;
        Dictionary<string, Type> columns 
          = new Dictionary<string, Type>();
        string tableCreationAssuranceQuery;
        if (CachedTableQueries.TryGetValue(tableName, out var tableValues)) {
          columnsToFetch = tableValues.Item1;
          tableCreationAssuranceQuery = tableValues.Item2;
        } else {
          // image table columns:
          if (tableName == ModelImageFieldAttribute.ImageTableName) {
            columns = ModelImageFieldAttribute.ImageTableColumns;
          } // model defined columns:
          else {
            columns = sqlFieldsForThisModelType.ToDictionary(
              field => field.dataModelFieldInfo.Name ?? field.objectFieldInfo.Name,
              field => field.dataModelFieldInfo.CustomSerializeToType ?? field.objectFieldInfo.DataType()
            );
            // data column:
            columns[SQLJSONDataColumnName] = typeof(string);
          }

          // query text generation:
          columnsToFetch = columns.Keys;
          tableCreationAssuranceQuery
            = $"CREATE TABLE IF NOT EXISTS {tableName} (\n\t" +
                columns.Select(column => $"{column.Key} {(SqliteColumnDataTypeMappings.TryGetValue(column.Value, out string columnTypeName) ? columnTypeName : throw new NotImplementedException($"type {column.Value} is not recoginized as a SQL column type in SQLColumnDataTypeMappings"))} {(column.Key == SQLIDColumnName ? "PRIMARY KEY " : "")}").Join(SqlListDelimiter) +
              $"\n);\n\n";
        }

        CachedTableQueries[tableName] = (columnsToFetch, tableCreationAssuranceQuery);
        return (columnsToFetch, tableCreationAssuranceQuery, columns);
      }
    }

    #endregion

    #region TESTING

    /// <summary>
    /// do something on each model type currently loaded
    /// </summary>
    public static void ForEachLoadedModelType(Action<Type, DataModelAttribute, List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)>> action) {
      foreach (Type type in AppDomain.CurrentDomain.GetAssemblies().SelectMany(assembly => assembly.GetTypes()).Where(type => !type.IsAbstract)) {
        if (type.GetCustomAttributes(typeof(DataModelAttribute), true).FirstOrDefault() is DataModelAttribute dataModelInfo) {
          action(type, dataModelInfo, Cache.GetFieldsInfo(type));
        }
      }
    }

    /// <summary>
    /// Run a test for each model type
    /// </summary>
    public static void RunModelTests() =>
      ForEachLoadedModelType((type, dataModelInfo, _) => {
        try {
          UnityEngine.Debug.Log($"TEST: Start | SerializationAndDeserialization test for {type.FullName}");
          if (dataModelInfo.test_SerializationAndDeserialization(type)) {
            UnityEngine.Debug.Log($"TEST: Passed | SerializationAndDeserialization test for {type.FullName}");
          } else {
            UnityEngine.Debug.LogWarning($"TEST: Skipped | SerializationAndDeserialization test for {type.FullName}");
          }
        } catch (Exception e) {
          UnityEngine.Debug.LogError($"TEST: Failed | SerializationAndDeserialization test for {type.FullName}\nExeption was thrown\n{e.StackTrace}");
          throw e;
        }
      });

    #endregion

    /**
     * Data structire for a serialized model's data, or for deleting models
     */
    public struct SerializedData {

      /// <summary>
      /// If this serialized data item actually signifies that this is for deleting a bunch of child models before serializing the new ones
      /// </summary>
      public readonly bool isForChildDeletion;

      /// <summary>
      /// Table for this model
      /// </summary>
      public readonly string tableName;

      /// <summary>
      /// Serialized SQL row data
      /// </summary>
      public readonly Dictionary<string, object> sqlData;

      /// <summary>
      /// Serialized Json Data 
      /// </summary>
      public readonly JObject jsonData;

      /// <summary>
      /// Make a serialized data item based on a model to save to the DB in a row
      /// </summary>
      public SerializedData(string tableName, Dictionary<string, object> sqlColumnsData, JObject jsonData) {
        this.tableName = tableName;
        sqlData = sqlColumnsData;
        this.jsonData = jsonData;
        isForChildDeletion = false;
      }

      /// <summary>
      /// Make a serialized data item that tells the system to delete rows based on an owenr id and child model type
      /// </summary>
      SerializedData(string ownerId, string childTableName, string childFieldName) {
        tableName = childTableName;
        sqlData = new Dictionary<string, object> {
          {SQLOwnerColumnName, ownerId },
          {SQLOwnerModelFieldNameColumnName, childFieldName }
        };
        jsonData = null;
        isForChildDeletion = true;
      }

      /// <summary>
      /// Named usage of the above ctor
      /// </summary>
      internal static SerializedData MakeDeletionEntry(string ownerId, string childTableName, string childFieldName) {
        return new SerializedData(ownerId, childTableName, childFieldName);
      }

      /// <summary>
      /// Set the parent model field if it's empty:
      /// </summary>
      internal SerializedData setParentModelFieldIfEmpty(string parentModelFieldName) {
        if (sqlData.TryGetValue(SQLOwnerModelFieldNameColumnName, out object value)) {
          sqlData[SQLOwnerModelFieldNameColumnName] = string.IsNullOrEmpty(value as string) 
            ? parentModelFieldName 
            : value;
        } else {
          sqlData[SQLOwnerModelFieldNameColumnName] = parentModelFieldName;
        }

        return this;
      }

      internal void Deconstruct(out Dictionary<string, object> sqlColumnsData, out JObject jsonData) {
        sqlColumnsData = sqlData;
        jsonData = this.jsonData;
      }

      internal void Deconstruct(out string tableName, out Dictionary<string, object> sqlColumnsData, out JObject jsonData, out bool isForChildDeletion) {
        tableName = this.tableName;
        sqlColumnsData = sqlData;
        jsonData = this.jsonData;
        isForChildDeletion = this.isForChildDeletion;
      }

      internal void Deconstruct(out string tableName, out Dictionary<string, object> sqlColumnsData, out JObject jsonData) {
        tableName = this.tableName;
        sqlColumnsData = sqlData;
        jsonData = this.jsonData;
      }

      public static implicit operator SerializedData((string tableName, Dictionary<string, object> sqlParams, JObject jSONData) data) {
        return new SerializedData(data.tableName, data.sqlParams, data.jSONData);
      }
    }
  }
}
