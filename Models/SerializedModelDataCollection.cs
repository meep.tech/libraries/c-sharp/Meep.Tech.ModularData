﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Meep.Tech.ModularData {

  public partial class ModelSerializer {

    /// <summary>
    /// Store serialized data by table name then by id key.
    /// Helper class.
    /// </summary>
    public sealed class SerializedModelDataCollection 
      : Dictionary<string, Dictionary<string, IEnumerable<SerializedData>>> {

      /// Special Keys:
      ///<summary>
      /// Used to represent non unique models without an id 
      ///</summary>
      public const string NoIdDataKey = "NOID";


      ///<summary>
      ///Used to delete entries in a table by parent model id
      ///</summary>
      public const string DeleteDataKey = "DELETE";

      /// <summary>
      /// Shortcut helper to set the name of the field this model was on in the parent model.
      /// </summary>
      public void updateSqlOwnerFieldNameFieldForFirstSerializedDataItemFor(string modelId, string tableName, string parentModelFieldName) {
        var baseModelData = this[tableName][modelId].First();
        baseModelData.sqlData[SQLOwnerModelFieldNameColumnName] = parentModelFieldName;
        overwriteFirstSerializedDataItemFor(modelId, baseModelData);
      }

      /// <summary>
      /// Shortcut helper to add a json field to the base/first serialized value of a model
      /// </summary>
      public void appendJsonFieldToFirstSerializedDataItemFor(string modelId, string tableName, string fieldName, JToken fieldValue) {
        var baseModelData = this[tableName][modelId].First();
        baseModelData.jsonData.Add(fieldName, fieldValue);
        overwriteFirstSerializedDataItemFor(modelId, baseModelData);
      }

      /// <summary>
      /// Update each serialized element and return a new 
      /// </summary>
      internal SerializedModelDataCollection updateEach(Func<SerializedData, SerializedData> action) {
        SerializedModelDataCollection result = new SerializedModelDataCollection();
        foreach (string tableName in Keys) {
          foreach(string modelId in this[tableName].Keys) {
            IEnumerable<SerializedData> items = this[tableName][modelId].ForEach(action);
            result.addSerializedModelsToCollectionByTableNameThenId(items);
          }
        }

        return this;
      }

      /// <summary>
      /// Helper to update/overwite/replace the first data item for the given model.id
      /// </summary>
      void overwriteFirstSerializedDataItemFor(string modelId, SerializedData firstItem) {
        if (TryGetValue(firstItem.tableName, out Dictionary<string, IEnumerable<SerializedData>> serializedModelsById)) {
          if (serializedModelsById.TryGetValue(modelId, out IEnumerable<SerializedData> serializedDatas)) {
            this[firstItem.tableName][modelId] = serializedDatas.Skip(1).Prepend(firstItem);
          } else {
            this[firstItem.tableName][modelId] = firstItem.AsEnumerable();
          }
        } else {
          this[firstItem.tableName] = new Dictionary<string, IEnumerable<SerializedData>> {
            {modelId, firstItem.AsEnumerable()}
          };
        }
      }

      /// <summary>
      /// Helper to collect serialized models by table name and Id
      /// </summary>
      internal void prependSerializedModelToCollectionByTableNameThenId(string modelId, SerializedData modelToAdd) {
        if (TryGetValue(modelToAdd.tableName, out Dictionary<string, IEnumerable<SerializedData>> serializedModelsById)) {
          if (serializedModelsById.TryGetValue(modelId, out IEnumerable<SerializedData> serializedDatas)) {
            this[modelToAdd.tableName][modelId] = serializedDatas.Prepend(modelToAdd);
          } else {
            this[modelToAdd.tableName][modelId] = modelToAdd.AsEnumerable();
          }
        } else {
          this[modelToAdd.tableName] = new Dictionary<string, IEnumerable<SerializedData>> {
            {modelId, modelToAdd.AsEnumerable()}
          };
        }
      }

      /// <summary>
      /// Helper to collect serialized models by table name and Id
      /// </summary>
      internal void addSerializedModelToCollectionByTableNameThenId(string modelId, SerializedData modelToAdd) {
        if (TryGetValue(modelToAdd.tableName, out Dictionary<string, IEnumerable<SerializedData>> serializedModelsById)) {
          if (serializedModelsById.TryGetValue(modelId, out IEnumerable<SerializedData> serializedDatas)) {
            this[modelToAdd.tableName][modelId] = serializedDatas.Append(modelToAdd);
          } else {
            this[modelToAdd.tableName][modelId] = modelToAdd.AsEnumerable();
          }
        } else {
          this[modelToAdd.tableName] = new Dictionary<string, IEnumerable<SerializedData>> {
            {modelId, modelToAdd.AsEnumerable()}
          };
        }
      }

      /// <summary>
      /// Helper to collect serialized models by table name and Id
      /// </summary>
      internal void addSerializedModelToCollectionByTableNameThenId(SerializedData modelToAdd) {
        string modelId = modelToAdd.sqlData != null ? (string)(modelToAdd.sqlData.TryGetValue(SQLIDColumnName, out object foundID) ? foundID : NoIdDataKey) : NoIdDataKey;
        if (TryGetValue(modelToAdd.tableName, out Dictionary<string, IEnumerable<SerializedData>> serializedModelsById)) {
          if (serializedModelsById.TryGetValue(modelId, out IEnumerable<SerializedData> serializedDatas)) {
            this[modelToAdd.tableName][modelId] = serializedDatas.Append(modelToAdd);
          } else {
            this[modelToAdd.tableName][modelId] = modelToAdd.AsEnumerable();
          }
        } else {
          this[modelToAdd.tableName] = new Dictionary<string, IEnumerable<SerializedData>> {
            {modelId, modelToAdd.AsEnumerable()}
          };
        }
      }

      /// <summary>
      /// Helper to collect serialized models by table name and Id
      /// </summary>
      internal void addSerializedModelsToCollectionByTableNameThenId(IEnumerable<SerializedData> modelsToAdd) {
        modelsToAdd.ForEach(modelToAdd => {
          string modelId = modelToAdd.sqlData != null ? (string)(modelToAdd.sqlData.TryGetValue(SQLIDColumnName, out object foundID) ? foundID : NoIdDataKey) : NoIdDataKey;
          if (TryGetValue(modelToAdd.tableName, out Dictionary<string, IEnumerable<SerializedData>> serializedModelsById)) {
            if (serializedModelsById.TryGetValue(modelId, out IEnumerable<SerializedData> serializedDatas)) {
              this[modelToAdd.tableName][modelId] = serializedDatas.Append(modelToAdd);
            } else {
              this[modelToAdd.tableName][modelId] = modelToAdd.AsEnumerable();
            }
          } else {
            this[modelToAdd.tableName] = new Dictionary<string, IEnumerable<SerializedData>> {
              {modelId, modelToAdd.AsEnumerable()}
            };
          }
        });
      }
    }
  }
}
