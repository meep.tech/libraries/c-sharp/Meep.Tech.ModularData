﻿using System.Collections.Generic;

namespace Meep.Tech.ModularData {

  /// <summary>
  /// A model that can be saved to or loaded from the DB
  /// </summary>
  public interface IModel {

    /// <summary>
    /// (optional)Finish deserializing the model
    /// </summary>
    virtual void finishDeserialization() { }

    /// <summary>
    /// Convert this model's data to a serialized state.
    /// </summary>
    virtual ModelSerializer.SerializedModelDataCollection serialize(bool andAllChildrenRecursively = true) {
      return ModelSerializer.Serialize(
        this,
        andAllChildrenRecursively
      );
    }

    /// <summary>
    /// Deserialize this model from the given data.
    /// </summary>
    virtual IModel deserialize(
      Dictionary<string, object> serializedModelSQLFields,
      Newtonsoft.Json.Linq.JObject serializedModelJSONFields
    ) {
      var model = ModelSerializer.Deserialize(
        this,
        serializedModelSQLFields,
        serializedModelJSONFields
      );

      finishDeserialization();
      if (GetType().IsValueType && !model.Equals(this)) {
        model.finishDeserialization();
      }

      return model;
    }

    /// <summary>
    /// Get the table name used by the model
    /// </summary>
    /// <returns></returns>
    string getTableName() 
      => ModelSerializer.Cache.GetDataModelInfo(this).TableName;
  }
}
