﻿using System;

namespace Meep.Tech.ModularData {

  /// <summary>
  /// Modular Debugger.
  /// </summary>
  public static class Debugger {

    /// <summary>
    /// Log a simple message to the console
    /// </summary>
    public static Action<string> Log {
      get;
      set;
    } = message
      => Console.WriteLine(message);

    /// <summary>
    /// Log a simple message to the console
    /// </summary>
    public static Action<string> LogWarning {
      get;
      set;
    } = message
      => Console.WriteLine("Warning! :: " + message);

    /// <summary>
    /// Log a simple message to the console
    /// </summary>
    public static Action<string> LogError {
      get;
      set;
    } = message
      => Console.WriteLine("ERROR! :: " + message);

    /// <summary>
    /// Log a simple message to the console
    /// </summary>
    public static Action<string, Exception> LogErrorAndThrow {
      get;
      set;
    } = (message, exception)
      => throw new Exception(message, exception);
  }
}
