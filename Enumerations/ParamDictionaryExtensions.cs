﻿using System;
using System.Collections.Generic;
//TODO: remove unity usages:

namespace Meep.Tech.ModularData {

  /// <summary>
  /// Helpers for parameter dictionary access
  /// </summary>
  public static class ParamDictionaryExtensions {

    /// <summary>
    /// Fetch a param from a collection, or the default if it's not provided, or the provided is a nullable and null is provided
    /// </summary>
    public static T GetAs<T>(this Dictionary<Param, object> @params, Param toFetch, T defaultValue = default) {
      if (@params.TryGetValue(toFetch, out object value)) {
        // if the value is of the requested type, return it
        if (value is T typedValue) {
          return typedValue;
        }

        // if the provided value is null, and this is nullable, return the default value
        bool canBeNull = !toFetch.ValueType.IsValueType || (Nullable.GetUnderlyingType(toFetch.ValueType) != null);
        if (canBeNull && value == null) {
          return defaultValue;
        }

        // See if this can be cast to the valuetype of the param, and return it if it can.
        try {
          return (T)value.CastTo(toFetch.ValueType);
        } catch (Exception e) {
          throw new ParamMissmatchException($"Tried to get param as type {typeof(T).FullName}. But param has type {value?.GetType().FullName ?? "null"}, and should be type {toFetch.ValueType}.\n{e}");
        }
      }

      return defaultValue;
    }

    /// <summary>
    /// Fetch a param from a collection. The param cannot be left out, and no defaults will be replaced.
    /// </summary>
    public static T GetAndValidateAs<T>(this Dictionary<Param, object> @params, Param toFetch) {
      if (@params.TryGetValue(toFetch, out object value)) {
        // if the value is of the requested type, return it
        if (value is T typedValue) {
          return typedValue;
        }

        // if the provided value is null, and this is nullable, return the provided null
        bool canBeNull = !toFetch.ValueType.IsValueType || (Nullable.GetUnderlyingType(toFetch.ValueType) != null);
        if (canBeNull && value == null) {
          return default;
        }

        // See if this can be cast to the valuetype of the param, and return it if it can.
        try {
          return (T)value.CastTo(toFetch.ValueType);
        } catch (Exception e) {
          throw new ParamMissmatchException($"Tried to get param as type {typeof(T).FullName}. But param has type {value?.GetType().FullName ?? "null"}, and should be type {toFetch.ValueType}.\n{e}");
        }
      }

      throw new MissingParamException($"Tried to construct a model without the required param: {toFetch} of type {typeof(T).FullName} being provided. If this is a test, try adding a default value for the empty model for this required param to the Archetype's DefaultEmptyParams field.");
    }
  }

  /// <summary>
  /// Exception for a missing required parameter
  /// </summary>
  public class MissingParamException : MissingMemberException {
    public MissingParamException(string message) : base(message) { }
  }

  /// <summary>
  /// Exception for a param of the wrong type
  /// </summary>
  public class ParamMissmatchException : Exception {
    public ParamMissmatchException(string message) : base(message) { }
  }

  /// <summary>
  /// Can be used for preventing a mappable type from retries while being built
  /// </summary>
  public class DoNotBuildThisTypeException : InvalidOperationException {
    public DoNotBuildThisTypeException(string message) : base(message) { }
  }
}
