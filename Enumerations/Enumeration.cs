﻿using System;
using System.Linq;
using System.Collections.Generic;
// TODO: Remove unityengine dependencey here and everywheee else outside of meep.tech.games.unity and Assembly-CSharp.csproj
using UnityEngine;

namespace Meep.Tech.ModularData {

  /// <summary>
  /// Slow enum base type for simple ID registration
  /// </summary>
  public abstract class Enumeration : IMappable, IComparable, IEquatable<Enumeration> {

    #region Constants

    /// <summary>
    /// The names of all the registered types of Enumerations.
    /// </summary>
    public static IEnumerable<string> AllTypes
      => EnumsByExternalId.Keys;

    #region All Enum Collectors

    /// <summary>
    /// The max assigned enum id for each type
    /// </summary>
    static Dictionary<string, int> NextFreeIds
      = new Dictionary<string, int>();

    /// <summary>
    /// The enums in this type by id
    /// </summary>
    static Dictionary<string, Dictionary<int, Enumeration>> EnumsById
      = new Dictionary<string, Dictionary<int, Enumeration>>();

    /// <summary>
    /// The enums in this type by name
    /// </summary>
    static Dictionary<string, Dictionary<string, Enumeration>> EnumsByName
      = new Dictionary<string, Dictionary<string, Enumeration>>();

    /// <summary>
    /// The enums in this type by name
    /// </summary>
    static Dictionary<string, Dictionary<string, Enumeration>> EnumsByExternalId
      = new Dictionary<string, Dictionary<string, Enumeration>>();

    #endregion

    #endregion

    /// <summary>
    /// The integer id behind the emum.
    /// Used for internal comparison, set on registration at start-up. Not always consistent between runs.
    /// </summary>
    [BaseEnumArchetypeField]
    public int Id {
      get;
      internal set;
    }

    /// <summary>
    /// The enum's display name
    /// </summary>
    [BaseEnumArchetypeField]
    public string Name {
      get;
      internal set;
    }

    /// <summary>
    /// The custom external ID of this enum.
    /// Consistent always.
    /// </summary>
    [BaseEnumArchetypeField]
    public string ExternalId {
      get;
      internal set;
    }

    /// <summary>
    /// The enumeration type this is a part of
    /// </summary>
    public Type Type {
      get;
    }

    #region Initialization

    /// <summary>
    /// Make a new simple enumeration that can be stored in the DB and extended via mods
    /// </summary>
    protected internal Enumeration(string externalId, string name, Type enumType) {
      if (!EnumsByExternalId.TryGetValue(enumType.FullName, out Dictionary<string, Enumeration> existingExternalEnumIds)) {
        RegisterNewEnumType(enumType);
        existingExternalEnumIds = EnumsByExternalId[enumType.FullName];
      }

      Id = NextFreeIds[enumType.FullName];
      ExternalId = MakeExternalId(externalId);
      Name = name;
      Type = enumType;

      // store new values if it doesn't exist
      Register(this, existingExternalEnumIds);
    }

    /// <summary>
    /// Make a new sub type of a given type using an enum
    /// TODO: maybe make each collection finalizeable so that it can be locked, and this throws an exception when used after they're locked.
    /// </summary>
    /// <returns></returns>
    public static Enumeration MakeSubtype(Enumeration baseEnumeration, Enumeration variableSubEnum) {
      Enumeration newSubEnumType = (Enumeration)baseEnumeration.MemberwiseClone();
      newSubEnumType.ExternalId = MakeExternalId(baseEnumeration.ExternalId, variableSubEnum);
      newSubEnumType.Name = $"{variableSubEnum.Name} {baseEnumeration.Name}";
      newSubEnumType.Id = NextFreeIds[baseEnumeration.Type.FullName];
      Register(newSubEnumType);

      return newSubEnumType;
    }

    /// <summary>
    /// Make an external id
    /// </summary>
    static string MakeExternalId(string externalIdBase, Enumeration variableSubType = null, bool condense = true) {
      string externalId;

      // Variations need different external ids
      if (variableSubType != null) {
        externalId = $"{externalIdBase}+{variableSubType.Name}";
      } else {
        externalId = externalIdBase;
      }

      return condense ? externalId.Replace(" ", string.Empty) : externalId;
    }

    /// <summary>
    /// Register this Enumeration
    /// </summary>
    static void Register(Enumeration enumeration, Dictionary<string, Enumeration> existingExternalEnumIds = null) {
      existingExternalEnumIds ??= EnumsByExternalId[enumeration.Type.FullName];

      if (existingExternalEnumIds.TryAdd(enumeration.ExternalId, enumeration)) {
        try {
          EnumsById[enumeration.Type.FullName].Add(enumeration.Id, enumeration);
          EnumsByName[enumeration.Type.FullName].Add(enumeration.Name, enumeration);
          NextFreeIds[enumeration.Type.FullName] = ++NextFreeIds[enumeration.Type.FullName];
        } catch (System.ArgumentException e) {
          throw new System.ArgumentException($"May already be a value of the given type in one of the arrays:\nValues:\n{enumeration.ExternalId}\n{enumeration.Name}\n{enumeration.Id}\nExisting Enums:\n{string.Join("\n", EnumsByName[enumeration.Type.FullName].Values.Select(e => e.ToString()))}\n{e}");
        }
        // if there's weird issues: 
      } else if (!existingExternalEnumIds.ContainsKey(enumeration.ExternalId)) throw new Exception($"Could not add new enum {enumeration.ExternalId} to {enumeration.Type}. Unknown issue. Failed on TryAdd but the key is not found in the dictionary?");
      else if (existingExternalEnumIds[enumeration.ExternalId].Name != enumeration.Name) throw new Exception($"A different Enumeration of type {enumeration.Type} with different Name: {existingExternalEnumIds[enumeration.ExternalId].Name} already exists with external id: {enumeration.ExternalId}");
      // just re-init the values if it already exists:
      else enumeration.Id = existingExternalEnumIds[enumeration.ExternalId].Id;
    }

    /// <summary>
    /// Get a combined enum for the given base and sub enum type
    /// </summary>
    protected static Enumeration GetCombinedEnum(Enumeration mainEnum, Enumeration subEnum) {
      string combinedExternalId = MakeExternalId(mainEnum.ExternalId, subEnum, false);
      return GetByExternalId(combinedExternalId, mainEnum.Type);
    }

    /// <summary>
    /// Set up a new enum type
    /// </summary>
    /// <param name="enumType"></param>
    static void RegisterNewEnumType(Type enumType) {
      NextFreeIds.Add(enumType.FullName, 0);
      EnumsById.Add(enumType.FullName, new Dictionary<int, Enumeration>());
      EnumsByName.Add(enumType.FullName, new Dictionary<string, Enumeration>());
      EnumsByExternalId.Add(enumType.FullName, new Dictionary<string, Enumeration>());
    }

    #endregion

    #region Enum Lookup Accessors

    /// <summary>
    /// Get all the enumerations of the given type
    /// </summary>
    public static IEnumerable<EnumType> GetAll<EnumType>() where EnumType : Enumeration {
      return EnumsByExternalId[typeof(EnumType).FullName].Values.Cast<EnumType>();
    }

    /// <summary>
    /// Get all the enumerations of the given type
    /// </summary>
    public static IEnumerable<Enumeration> GetAll(Type enumType){
      return EnumsByExternalId[enumType.FullName].Values as IEnumerable<Enumeration>;
    }

    /// <summary>
    /// Get all the enumerations of the given type
    /// </summary>
    public static IEnumerable<Enumeration> GetAllForType(string enumTypeName) {
      return EnumsByExternalId[enumTypeName].Values as IEnumerable<Enumeration>;
    }

    /// <summary>
    /// Get all the enumerations of the given type
    /// </summary>
    public static Enumeration GetForType(string externalId, string enumTypeName) {
      return EnumsByExternalId[enumTypeName][externalId];
    }

    /// <summary>
    /// Get the enum value by name
    /// </summary>
    /// <returns></returns>
    public static EnumType GetById<EnumType>(int enumId) where EnumType : Enumeration {
      return EnumsById.TryGetValue(typeof(EnumType).FullName, out Dictionary<int, Enumeration> enumsNamesForType)
        ? enumsNamesForType.TryGetValue(enumId, out Enumeration enumeration)
          ? (EnumType)enumeration
          : throw new MissingFieldException($"No enum with name: {enumId} is registered for type:{typeof(EnumType).Name}.\n Valid Values: {string.Join("\n", EnumsById[typeof(EnumType).FullName].Keys)}.")
        : throw new MissingFieldException($"No enum of type: {typeof(EnumType).Name} is registered. Valid types include {string.Join("\n", EnumsByExternalId.Keys)}");
    }

    /// <summary>
    /// Get the enum value by name
    /// </summary>
    /// <returns></returns>
    internal static Enumeration GetById(int enumId, Type enumType) {
      return EnumsById.TryGetValue(enumType.FullName, out Dictionary<int, Enumeration> enumsNamesForType)
        ? enumsNamesForType.TryGetValue(enumId, out Enumeration enumeration)
          ? enumeration
          : throw new MissingFieldException($"No enum with name: {enumId} is registered for type:{enumType.Name}.\n Valid Values: {string.Join("\n", EnumsById[enumType.FullName].Keys)}.")
        : throw new MissingFieldException($"No enum of type: {enumType.Name} is registered. Valid types include {string.Join("\n", EnumsByExternalId.Keys)}");
    }

    /// <summary>
    /// Get the enum value by name
    /// </summary>
    /// <returns></returns>
    public static EnumType GetByName<EnumType>(string enumName) where EnumType : Enumeration {
      return EnumsByName.TryGetValue(typeof(EnumType).FullName, out Dictionary<string, Enumeration> enumsNamesForType)
        ? enumsNamesForType.TryGetValue(enumName, out Enumeration enumeration)
          ? (EnumType)enumeration
          : throw new MissingFieldException($"No enum with name: {enumName} is registered for type:{typeof(EnumType).Name}.\n Valid Values: {string.Join("\n", EnumsByName[typeof(EnumType).FullName].Keys)}.")
        : throw new MissingFieldException($"No enum of type: {typeof(EnumType).FullName} is registered. Valid types include {string.Join("\n", EnumsByExternalId.Keys)}");
    }

    /// <summary>
    /// Get the enum value by name
    /// </summary>
    /// <returns></returns>
    public static EnumType GetByExternalId<EnumType>(string enumExternalId) where EnumType : Enumeration {
      return EnumsByExternalId.TryGetValue(typeof(EnumType).FullName, out Dictionary<string, Enumeration> enumsNamesForType)
        ? enumsNamesForType.TryGetValue(enumExternalId, out Enumeration enumeration)
          ? (EnumType)enumeration
          : throw new MissingFieldException($"No enum with external id: {enumExternalId} is registered for type:{typeof(EnumType).Name}.\n Valid Values: {string.Join("\n", EnumsByExternalId[typeof(EnumType).FullName].Keys)}.")
        : throw new MissingFieldException($"No enum of type: {typeof(EnumType).FullName} is registered. Valid types include {string.Join("\n", EnumsByExternalId.Keys)}");
    }

    /// <summary>
    /// Get the enum value by name
    /// </summary>
    /// <returns></returns>
    internal static Enumeration GetByExternalId(string enumExternalId, Type enumType) {
      return EnumsByExternalId.TryGetValue(enumType.FullName, out Dictionary<string, Enumeration> enumsNamesForType)
        ? enumsNamesForType.TryGetValue(enumExternalId, out Enumeration enumeration)
          ? enumeration
          : throw new MissingFieldException($"No enum with external id: {enumExternalId} is registered for type:{enumType.Name}.\n Valid Values: {string.Join("\n", EnumsByExternalId[enumType.FullName].Keys)}.")
        : throw new MissingFieldException($"No enum of type: {enumType.FullName} is registered. Valid types include {string.Join("\n", EnumsByExternalId.Keys)}");
    }

    /// <summary>
    /// TODO: REMOVE THIS BEFORE PUBLISHING
    /// </summary>
    /// <param name="mappableTypeClass"></param>
    public static void Reset(Type mappableTypeClass) {
      RemoveEnumType(mappableTypeClass);
    }

    /// <summary>
    /// Set up a new enum type
    /// </summary>
    /// <param name="enumType"></param>
    static void RemoveEnumType(Type enumType) {
      NextFreeIds.Remove(enumType.FullName);
      EnumsById.Remove(enumType.FullName);
      EnumsByName.Remove(enumType.FullName);
      EnumsByExternalId.Remove(enumType.FullName);
    }

    #endregion

    #region Comparison and Conversion

    /// <summary>
    /// ==
    /// </summary>
    /// <param name="enum"></param>
    public static bool operator ==(Enumeration enum1, Enumeration enum2) {
      return enum1 is null ? enum2 is null : enum1.Equals(enum2);
    }

    /// <summary>
    /// !=
    /// </summary>
    /// <param name="enum"></param>
    public static bool operator !=(Enumeration enum1, Enumeration enum2) {
      return enum1 is null ? !(enum2 is null) : !enum1.Equals(enum2);
    }

    /// <summary>
    /// ==
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj) {
      if (!(obj is Enumeration otherValue)) {
        return false;
      }

      bool typeMatches = GetType().Equals(obj.GetType());
      bool valueMatches = Id.Equals(otherValue.Id);

      return typeMatches && valueMatches;
    }

    /// <summary>
    /// ==
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public int CompareTo(object other)
      => Id.CompareTo(((Enumeration)other).Id);

    /// <summary>
    /// ==
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public bool Equals(Enumeration other) {
      return !(other is null)
        && Id == other.Id
        && GetType() == other.GetType();
    }

    #endregion

    #region Serialization

    /// <summary>
    /// #
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode() {
      return Id.GetHashCode() * Type.Name.GetHashCode();
    }

    /// <summary>
    /// tostring
    /// </summary>
    /// <returns></returns>
    public override string ToString()
      => $"{Id}_/{ExternalId}/";

    #endregion

    #region Exceptions

    public class MissingEnumerableTypeDataException : Exception {
      internal MissingEnumerableTypeDataException(int missingId, IEnumerable<string> validValues, string enumTypeName) : base($"EnumerableType with id:{missingId} is not registered for {enumTypeName}."
        + $"\nValid Types:"
        + $"\n{string.Join("\n", validValues)}") {
      }
      internal MissingEnumerableTypeDataException(IEnumerable<string> validValues, string missingName, string enumTypeName) : base($"EnumerableType with name:{missingName} is not registered for {enumTypeName}."
        + $"\nValid Types:"
        + $"\n{string.Join("\n", validValues)}") {
      }
      internal MissingEnumerableTypeDataException(string externalId, IEnumerable<string> validValues, string enumTypeName) : base($"EnumerableType with externalId:{externalId} is not registered for {enumTypeName}."
        + $"\nValid Types:"
        + $"\n{string.Join("\n", validValues)}") {
      }
      internal MissingEnumerableTypeDataException(Enumeration missingId, IEnumerable<string> validValues, string enumTypeName) : base($"EnumerableType with enumID:{missingId} is not registered for {enumTypeName}."
        + $"\nValid Types:"
        + $"\n{string.Join("\n", validValues)}") {
      }
    }

    #endregion
  }
}