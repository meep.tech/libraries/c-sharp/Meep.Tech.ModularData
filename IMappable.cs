﻿namespace Meep.Tech.ModularData {

  /// <summary>
  /// Mappable types
  /// </summary>
  public interface IMappable {

    /// <summary>
    /// Mapable types have a unique integer id
    /// </summary>
    int Id {
      get;
    }

    /// <summary>
    /// Mapapable types need a name
    /// </summary>
    string Name {
      get;
    }

    /// <summary>
    /// Mapapable types need a name
    /// </summary>
    string ExternalId {
      get;
    }
  }
}